!*****************************************************************************
module HarmonicOscillator
!*****************************************************************************
!
!  Project: MolecularDynamics
!
!  Created on: Jan 2019 by Alfonso Gijón
!
! ---
! Copyright (C) 2018       Eduardo R. Hernandez - ICMM
!  This file is distributed under the terms of the
!  GNU General Public License: see COPYING in the top directory
!  or http://www.gnu.org/copyleft/gpl.txt .
! ---
!
! This module is only for testing/illustration purposes; it provides a
! public subroutine, evaluate_energy_HO, which evaluates the energy and force
! given the position of the oscillator
!
!*****************************************************************************
!  modules used

!*****************************************************************************
!  No implicits please!
   
      implicit none

!*****************************************************************************
!  private module variables

      integer, private, parameter :: i8b = selected_int_kind(18)
      integer, private, parameter :: sp = selected_real_kind(6,30)
      integer, private, parameter :: dp = selected_real_kind(14,100)

      real (dp), private :: koscillator 

!*****************************************************************************
!  private module subroutines

   ! none

contains

!*****************************************************************************
subroutine HO_setUp( k )
!*****************************************************************************
!
!  Project: MolecularDynamics    
!
!  Created on: Jan 2019 by Alfonso Gijón
!
! ---
! Copyright (C) 2018       Eduardo R. Hernandez - ICMM
!  This file is distributed under the terms of the
!  GNU General Public License: see COPYING in the top directory
!  or http://www.gnu.org/copyleft/gpl.txt .
! ---
! 
! Allows user to define the k-constant of the harmonic potential
!
! V(r) = 0.5 * k * r^2
!
!*****************************************************************************
!  modules used

!*****************************************************************************
!  No implicits please!
   
      implicit none

!*****************************************************************************
!  variables

      real (dp), intent (IN) :: k

!*****************************************************************************
!  local variables

!*****************************************************************************
!  start of subroutine

      koscillator = k

end subroutine HO_setUp

!*****************************************************************************
subroutine evaluate_energy_HO( natoms, position, energy, force )
!*****************************************************************************
!
!  Project: MolecularDynamics
!
!  Created on: Jan 2019 by Alfonso Gijón
!
! ---
! Copyright (C) 2018       Eduardo R. Hernandez - ICMM
!  This file is distributed under the terms of the
!  GNU General Public License: see COPYING in the top directory
!  or http://www.gnu.org/copyleft/gpl.txt .
! ---
!
! Given the position of the oscillator, calculate energy and force  
!
!*****************************************************************************
!  modules used

!*****************************************************************************
!  No implicits please!
   
      implicit none

!*****************************************************************************
!  variables

      integer, intent (IN) :: natoms
      
      real (dp), dimension(:,:), intent (IN) :: position

      real (dp), intent (OUT) :: energy
      real (dp), dimension(:,:), intent (OUT) :: force

!*****************************************************************************
!  local variables

      real (dp), parameter :: zero = 0.0d0
      real (dp), parameter :: half = 0.5d0

      integer :: i
      
!*****************************************************************************
!  start subroutine

      energy = zero
      
      do i = 1, natoms
         energy = energy + half * koscillator * dot_product( position(:,i), position(:,i) )
         force(:,i) = - koscillator * position(:,i)
      end do

end subroutine evaluate_energy_HO

end module HarmonicOscillator
