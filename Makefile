
NAME1 = NVE
NAME2 = NVT
NAME3 = NPT
NAME4 = NPTfcc
#NAME5 = NPTshell
NAME6 = PI_HO
NAME7 = PI_water_qTIP4Pf
NAME8 = PI_water_SPCF

all: modules ${NAME1} ${NAME2} ${NAME3} ${NAME4} ${NAME5} ${NAME6} ${NAME7} ${NAME8}

FC = gfortran
OFLAGS = -O2
DFLAGS = #-pg #-fcheck='all' -Wall -Wextra -g -pedantic-errors -Wsurprising -Wunderflow -Wintrinsic-shadow -fbacktrace -fimplicit-non
OMPFLAGS = -fopenmp

#FC = /opt/intel/compilers_and_libraries_2018.0.128/linux/bin/intel64/ifort
#FC = /opt/intel/bin/ifort
#OFLAGS = -O2
#DFLAGS = #-pg #-fcheck='all' -Wall -Wextra -g -pedantic-errors -Wsurprising -Wunderflow -Wintrinsic-shadow -fbacktrace -fimplicit-non
#OMPFLAGS = -qopenmp

OFILES = LennardJones.o \
         HarmonicOscillator.o \
         qTIP4Pf.o \
         SPCF.o

MDOFILES = MD_State.o MolecularDynamics.o

${NAME1} : ${OFILES} ${MDOFILES} example_NVE.o
	${FC} ${OFLAGS} -o example_NVE ${DFLAGS} example_NVE.o ${OFILES} ${MDOFILES}

${NAME2} : ${OFILES} ${MDOFILES} example_NVT.o
	${FC} ${OFLAGS} -o example_NVT ${DFLAGS} example_NVT.o ${OFILES} ${MDOFILES}

${NAME3} : ${OFILES} ${MDOFILES} example_NPT.o
	${FC} ${OFLAGS} -o example_NPT ${DFLAGS} example_NPT.o ${OFILES} ${MDOFILES}

${NAME4} : ${OFILES} ${MDOFILES} example_NPT_LJfcc.o
	${FC} ${OFLAGS} -o example_NPT_LJfcc ${DFLAGS} example_NPT_LJfcc.o ${OFILES} ${MDOFILES}

#${NAME5} : ${OFILES} example_ShellNPT.o
#	${FC} ${OFLAGS} -o example_ShellNPT ${DFLAGS} example_ShellNPT.o ${OFILES}

${NAME6} : ${OFILES} ${MDOFILES} example_PI_HO.o
	${FC} ${OFLAGS} ${OMPFLAGS} -o example_PI_HO ${DFLAGS} example_PI_HO.o ${OFILES} ${MDOFILES}

${NAME7} : ${OFILES} ${MDOFILES} example_PI_water_qTIP4Pf.o
	${FC} ${OFLAGS} ${OMPFLAGS} -o example_PI_water_qTIP4Pf ${DFLAGS} example_PI_water_qTIP4Pf.o ${OFILES} ${MDOFILES}

${NAME8} : ${OFILES} ${MDOFILES} example_PI_water_SPCF.o
	${FC} ${OFLAGS} ${OMPFLAGS} -o example_PI_water_SPCF ${DFLAGS} example_PI_water_SPCF.o ${OFILES} ${MDOFILES}

MolecularDynamics.o: MolecularDynamics.f90 \
                     MD_State.o
	${FC} -c ${OFLAGS} ${DFLAGS} $<

LennardJones.o: LennardJones.f90
	${FC} -c ${OFLAGS} ${DFLAGS} $<

HarmonicOscillator.o: HarmonicOscillator.f90 
	${FC} -c ${OFLAGS} ${DFLAGS} $<

qTIP4Pf.o: qTIP4Pf.f90 
	${FC} -c ${OFLAGS} ${DFLAGS} $<

SPCF.o: SPCF.f90 
	${FC} -c ${OFLAGS} ${DFLAGS} $<

MD_State.o: MD_State.f90 
	${FC} -c ${OFLAGS} ${DFLAGS} $<

example_NVE.o: example_NVE.f90 \
               LennardJones.o \
               MolecularDynamics.o \
               MD_State.o
	${FC} -c ${OFLAGS} ${DFLAGS} $< 

example_NVT.o: example_NVT.f90 \
               LennardJones.o \
               MolecularDynamics.o \
               MD_State.o
	${FC} -c ${OFLAGS} ${DFLAGS} $< 

example_NPT.o: example_NPT.f90 \
               LennardJones.o \
               MolecularDynamics.o \
               MD_State.o
	${FC} -c ${OFLAGS} ${DFLAGS} $< 

example_NPT_LJfcc.o: example_NPT_LJfcc.f90 \
               LennardJones.o \
               MolecularDynamics.o \
               MD_State.o
	${FC} -c ${OFLAGS} ${DFLAGS} $<

#example_ShellNPT.o: example_ShellNPT.f90 \
#               MolecularDynamics.o \
#               MD_State.o
#	${FC} -c ${OFLAGS} ${DFLAGS} $< 

example_PI_HO.o: example_PI_HO.f90 \
           HarmonicOscillator.o \
           MolecularDynamics.o \
           MD_State.o
	${FC} -c ${OFLAGS} ${DFLAGS} $<

example_PI_water_qTIP4Pf.o: example_PI_water_qTIP4Pf.f90 \
           qTIP4Pf.o \
           MolecularDynamics.o \
           MD_State.o
	${FC} -c ${OFLAGS} ${DFLAGS} $<

example_PI_water_SPCF.o: example_PI_water_SPCF.f90 \
           SPCF.o \
           MolecularDynamics.o \
           MD_State.o
	${FC} -c ${OFLAGS} ${DFLAGS} $<

MODULE_DIRS = MD_State_dir \
              MolecularDynamics_dir

.PHONY: module_dirs ${MODULE_DIRS}

modules:
	for dir in ${MODULE_DIRS}; do \
	    $(MAKE) -s -C $$dir; \
	done

clean:
	rm *.o *.mod
