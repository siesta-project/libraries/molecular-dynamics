!*****************************************************************************
subroutine CentroidProperties( state_chain, centroid, radius2, rij, rij2, &
     radius0, density_o, density_h)
  
!*****************************************************************************
!
!  Project: MolecularDynamics
!
!  Module: State (PUBLIC routine)
!
!  Created on: May 2019 by Alfonso Gijón
!
! ---
! Copyright (C) 2018       Eduardo R. Hernandez - ICMM
!  This file is distributed under the terms of the
!  GNU General Public License: see COPYING in the top directory
!  or http://www.gnu.org/copyleft/gpl.txt .
! ---
!
! This subroutine calculates the radius of the centroid of the cluster, with
! respect to the center of masses. Also the intermolecular distances and the
! spatial distribution of the atoms are computed
!
!*****************************************************************************
!  modules used

      use omp_lib  

!*****************************************************************************
!  No implicits please!
   
      implicit none

!*****************************************************************************
!  variables

      type ( StateChain_type ), intent (INOUT) :: state_chain

      real (dp), dimension (:,:), intent(IN) :: centroid

      real(dp), intent (OUT) :: radius2

      real(dp), dimension(:,:), intent (OUT) :: rij

      real(dp), dimension(:,:), intent (OUT) :: rij2

      real(dp), optional, intent (IN) :: radius0

      real(dp), optional, dimension(:,:,:), intent (OUT) :: density_o

      real(dp), optional, dimension(:,:,:), intent (OUT) :: density_h      

!*****************************************************************************
!  local variables

      integer i, j
      integer i1, i2
      integer imol, jmol
      integer iat, jat

      integer nbinx
      
      integer nAtoms, nmols

      real(dp) :: mtotal, mh2o
      real (dp), dimension (3) :: rcm, deltar

      real (dp), allocatable, dimension (:) :: mass
      real (dp), allocatable, dimension(:,:) :: rcm_mol

      real(dp) :: dx, xmin, xmax, x1l, x1h, x2l, x2h, rx, ry, rz

!*****************************************************************************
!  start of subroutine

      if ( debug ) write(*,*) 'Entering CentroidProperties()'

      call getStateChain( state_chain, nAtoms = nAtoms )

      nmols = nAtoms/3
      
      allocate( mass( nAtoms ) )
      allocate( rcm_mol( 3, nmols ) )

      call getStateChain( state_chain, mass = mass )
      
! radius2
      
      ! center of masses of the centroid cluster
         
      rcm = zero
      mtotal = zero
      do j = 1, nAtoms
         rcm = rcm + mass(j)*centroid(:,j)
         mtotal = mtotal + mass(j)
      end do
      rcm = rcm / mtotal

      !print*, ""
      !print*, "CM", rcm
      !print*, ""

      ! mean distance to the center of masses

      radius2 = zero
      mh2o = mass(1) + two * mass(2)
         
      do imol = 1, nmols
         
         rcm_mol(:,imol) = zero
         do j = 1, 3
            iat = j + 3*(imol-1)
            rcm_mol(:,imol) = rcm_mol(:,imol) + mass(iat)*centroid(:,iat)
         end do
         rcm_mol(:,imol) = rcm_mol(:,imol) / mh2o

         deltar = rcm_mol(:,imol) - rcm
         radius2 = radius2 + dot_product( deltar, deltar )
            
      end do
      radius2 = radius2 / dfloat( nmols )

! intermolecular distances

      do imol = 1, nmols
         do jmol = imol+1, nmols
            deltar = rcm_mol(:,imol) - rcm_mol(:,jmol)
            rij2(imol,jmol) = dot_product( deltar, deltar )
            rij(imol,jmol) = dsqrt( rij2(imol,jmol) )
         end do
      end do

! spatial density of the centroid

      if ( present(density_o) .and. present(density_h) ) then

         nbinx = size( density_o(1,1,:) )
         
         xmax = two * radius0
         xmin = -xmax
         dx = (xmax-xmin)/dfloat(nbinx)
         
         density_o = zero
         density_h = zero         

         !$OMP PARALLEL &
         !$OMP PRIVATE( imol, j, iat, rx, ry, rz, i1, i2, x1l, x1h, x2l, x2h ) 
         !$OMP DO
         do imol = 1, nmols

            do j = 1, 3

               iat = j + 3*(imol-1)
               rx = centroid(1,iat) 
               ry = centroid(2,iat) 
               rz = centroid(3,iat) 
               
               if (j.eq.1) then    ! oxygen distribution

                  do i1 = 0, nbinx-1
                     x1l = xmin + dfloat(i1)*dx
                     x1h = x1l + dx
                     do i2 = 0, nbinx-1
                        x2l = xmin + dfloat(i2)*dx
                        x2h = x2l + dx
                        ! xy projection
                        if ( x1l.lt.rx .and. rx.lt.x1h .and. x2l.lt.ry .and. ry.lt.x2h ) then
                           density_o(1,i1+1,i2+1) = density_o(1,i1+1,i2+1) + one
                        end if
                        ! yz projection
                        if ( x1l.lt.ry .and. ry.lt.x1h .and. x2l.lt.rz .and. rz.lt.x2h ) then
                           density_o(2,i1+1,i2+1) = density_o(2,i1+1,i2+1) + one
                        end if
                        ! xz projection
                        if ( x1l.lt.rx .and. rx.lt.x1h .and. x2l.lt.rz .and. rz.lt.x2h ) then
                           density_o(3,i1+1,i2+1) = density_o(3,i1+1,i2+1) + one
                        end if                        
                     end do ! end i2
                  end do ! end i1

               else                ! hydrogen distribution

                  do i1 = 0, nbinx-1
                     x1l = xmin + dfloat(i1)*dx
                     x1h = x1l + dx
                     do i2 = 0, nbinx-1
                        x2l = xmin + dfloat(i2)*dx
                        x2h = x2l + dx
                        ! xy projection
                        if ( x1l.lt.rx .and. rx.lt.x1h .and. x2l.lt.ry .and. ry.lt.x2h ) then
                           density_h(1,i1+1,i2+1) = density_h(1,i1+1,i2+1) + one
                        end if
                        ! yz projection
                        if ( x1l.lt.ry .and. ry.lt.x1h .and. x2l.lt.rz .and. rz.lt.x2h ) then
                           density_h(2,i1+1,i2+1) = density_h(2,i1+1,i2+1) + one
                        end if
                        ! xz projection
                        if ( x1l.lt.rx .and. rx.lt.x1h .and. x2l.lt.rz .and. rz.lt.x2h ) then
                           density_h(3,i1+1,i2+1) = density_h(3,i1+1,i2+1) + one
                        end if                        
                     end do ! end i2
                  end do ! end i1
                  
               end if

            end do ! end j

         end do ! end imol
         !$OMP END DO
         !$OMP END PARALLEL
            
      end if
         
      deallocate( mass )
      deallocate( rcm_mol )      
      
      if ( debug ) write(*,*) 'Exiting CentroidProperties()'
      
end subroutine CentroidProperties
