!*****************************************************************************
program example_NVE
!*****************************************************************************
!
!  Project: MolecularDynamics
!
!  Created on: Thu 12 Apr 15:15:48 2018  by Eduardo R. Hernandez
!
!  Last Modified: Wed Apr  7 12:23:31 2021
!
! ---
! Copyright (C) 2018       Eduardo R. Hernandez - ICMM
!  This file is distributed under the terms of the
!  GNU General Public License: see COPYING in the top directory
!  or http://www.gnu.org/copyleft/gpl.txt .
! ---
!
! This means to serve as an example of a client program that runs an MD
! simulation using the MolecularDynamics module, combined with a simple
! Lennard-Jones potential.
!
!*****************************************************************************
!  modules used

      use LennardJones
      use MD_State_module
      use MolecularDynamics

!*****************************************************************************
!  No implicits please!
   
      implicit none

!*****************************************************************************
!  local variables

      integer i
      integer n0

      integer dyntype
      integer n_atoms
      integer nSteps 
      integer iStep

      logical start
      logical periodic
      logical fixCellShape

      real (dp) :: tMass 
      real (dp) :: bMass 

      real (dp) :: alj, blj

      real (dp) :: delta_t
      real (dp) :: kineticEnergy
      real (dp) :: potential_energy, potentialEnergy
      real (dp) :: totalEnergy
      real (dp) :: constant
      real (dp) :: temperature

      real (dp), dimension (3) :: a, b, c
      real (dp), allocatable, dimension (:,:) :: position
      real (dp), allocatable, dimension (:,:) :: force
      real (dp), allocatable, dimension (:) :: mass
      real (dp), dimension (3,3) :: stress

      character ( len = 30 ) :: restartFile
      character ( len = 30 ) :: newRestartFile

      type ( state_type ) :: state

!*****************************************************************************
!  begin program

      restartFile = 'RestartNVE'
      newRestartFile = 'RestartNVE_2'

      nSteps = 1000

      dyntype = 0  ! NVE velocity-Verlet dynamics
      
      start = .true.    ! this is a start simulation
      periodic = .true. ! we assume periodic-boundary conditions

      fixCellShape = .false. ! we do not impose a fixed cell shape

      tMass = 1.0d0
      bMass = 1.0d0

      temperature = 1000.0d0

      ! define the working units

      call setUnits( Bohr, Hartree )

      delta_t = 1.0d0 ! 1 fs

      a = (/60.55, 0.0, 0.0/)
      b = (/0.0, 60.55, 0.0/)
      c = (/0.0, 0.0, 60.55/)

      n_atoms = 500

      allocate( mass( n_atoms ) )
      allocate( position( 3, n_atoms ) )
      allocate( force( 3, n_atoms ) )

      open( unit = 5, file = 'coords.dat' )
      do i = 1, n_atoms
          read(5,*) position(1:3,i) 
          mass(i) = 39.631635050750646d0
      end do

!     if ( length .eq. 1 ) then ! cell parameters and atomic positions 
!                               ! must be in client units (Angstrom in this case)
!        a = a / Angstrom
!        b = b / Angstrom
!        c = c / Angstrom

!     end if

      ! create an empty instance of state

      call createState( state, n_atoms )

      if ( start ) then 

         call initialiseState( state, a, b, c, mass, position,  &
                        temperature = temperature, lattice = .true. )

      else 

         call readRestart( state, restartFile, n0 )

      end if

      ! after creating state, initialise the MD simulation

      call MD_setUp( state, dyntype, delta_t, temperature )

!     if ( ( energ .eq. 0 ) .and. ( length .eq. 0 ) ) then   ! atomic units
        alj = 7644167.8958719987d0
        blj = 107.70436055866918d0
!     else if ( ( energ .eq. 1 ) .and. ( length .eq. 0 ) ) then  ! Rydberg-Bohr
!       alj = 7644167.8958719987d0 * two
!       blj = 107.70436055866918d0 * two
!     else if ( ( energ .eq. 2 ) .and. ( length .eq. 1 ) ) then  ! eV-Angstrom
!       alj = 7644167.8958719987d0 / ( Angstrom**12 ) / eV 
!       blj = 107.70436055866918d0 / ( Angstrom**6 ) / eV
!     end if

      call LJ_setUp( alj, blj ) 

      ! before we can call evaluate_energy, we must get the current configuration from state

      call getConfiguration( state, n_atoms, a, b, c, position, lattice = .false. )
      call evaluate_energy_LJ( n_atoms, a, b, c, position, potential_energy, force, stress )
      call setForce( state, potential_energy, force, stress )

      do i = 1, nSteps

         iStep = n0 + i

         call MD_stepA( state )
         call getConfiguration( state, n_atoms, a, b, c, position, lattice = .false. )
         call evaluate_energy_LJ( n_atoms, a, b, c, position, potential_energy, force, stress )
         call setForce( state, potential_energy, force, stress )
         call MD_stepB( state )

         if ( mod( i, 1 ) .eq. 0 ) then
            call inquire( state, temperature = temperature, kineticEnergy = kineticEnergy, &
                          potentialEnergy = potentialEnergy )
            totalEnergy = kineticEnergy + potentialEnergy
            write(6,'(i5,f17.7,3f17.7)') iStep, temperature, kineticEnergy, potentialEnergy, totalEnergy
         end if

         if ( mod( i, 100 ) .eq. 0 ) call writeRestart( state, newRestartFile, iStep )

      end do

      call writeRestart( state, newRestartFile, iStep )

      deallocate( mass )
      deallocate( position )
      deallocate( force )

      call deleteState( state )

end program example_NVE
