!*****************************************************************************
module LennardJones
!*****************************************************************************
!
!  Project: MolecularDynamics
!
!  Created on: Wed 18 Apr 10:38:50 2018  by Eduardo R. Hernandez
!
!  Last Modified: Wed Apr  7 12:27:34 2021
!
! ---
! Copyright (C) 2018       Eduardo R. Hernandez - ICMM
!  This file is distributed under the terms of the
!  GNU General Public License: see COPYING in the top directory
!  or http://www.gnu.org/copyleft/gpl.txt .
! ---
!
! This module is only for testing/illustration purposes; it provides a
! public subroutine, evaluate_energy_LJ, which evaluates the energy, forces
! and stress, given a configuration of the system (cell and coordinates).
!
!*****************************************************************************
!  modules used

!     use MD_precision_module
!     use MD_constants_module

!*****************************************************************************
!  No implicits please!
   
      implicit none

!*****************************************************************************
!  private module variables

      integer, private, parameter :: i8b = selected_int_kind(18)
      integer, private, parameter :: sp = selected_real_kind(6,30)
      integer, private, parameter :: dp = selected_real_kind(14,100)

      real (dp), private, parameter :: four = 4.0d0
      real (dp), private, parameter :: six = 6.0d0
      real (dp), private, parameter :: sixth = 1.0d0 / 6.0d0
      real (dp), private, parameter :: twelve = 12.0d0
      real (dp), private, parameter :: zero = 0.0d0

      real (dp), private :: ALJ12 ! these are the LJ parameters
      real (dp), private :: BLJ6  ! VLJ(r) = ALJ12 / r^12 - BLJ6 / r^6 - LJ_shift
      real (dp), private :: LJ_shift ! shifts the potential to zero at r = rcut

      real (dp), private :: rcut ! potential cutoff
      real (dp), private :: rcut2 ! potential cutoff squared

!*****************************************************************************
!  private module subroutines

   ! none

contains

!*****************************************************************************
subroutine LJ_setUp( alj, blj )
!*****************************************************************************
!
!  Project: MolecularDynamics    
!
!  Created on: Wed 18 Apr 16:40:46 2018  by Eduardo R. Hernandez
!
!  Last Modified: Tue 22 Jan 11:23:17 2019
!
! ---
! Copyright (C) 2018       Eduardo R. Hernandez - ICMM
!  This file is distributed under the terms of the
!  GNU General Public License: see COPYING in the top directory
!  or http://www.gnu.org/copyleft/gpl.txt .
! ---
! 
! Allows user to define the cutoff for the LJ potential and to provide the 
! values of the defining parameters, ALJ12 and BLJ6. The potential for a  
! pair of interacting atoms is calculated as:
!
! VLJ(r) = ALJ12 / r^12 - BLJ6 / r^6 - LJ_shift
!
! where LJ_shift is the value of the potential evaluated at the cutoff, so
! that the potential is shifted to zero for r >= rcutoff.
!
!*****************************************************************************
!  modules used

!*****************************************************************************
!  No implicits please!
   
      implicit none

!*****************************************************************************
!  variables

      real (dp), intent (IN) :: alj
      real (dp), intent (IN) :: blj

!*****************************************************************************
!  local variables

      real (dp) :: r6

!*****************************************************************************
!  start of subroutine

      ALJ12 = alj
      BLJ6 = blj

      rcut = four * ( ALJ12 / BLJ6 )**sixth ! LJ cutoff, 4 * sigma
      rcut2 = rcut * rcut
      r6 = rcut2 * rcut2 * rcut2

      LJ_shift = ( ALJ12 / r6 - BLJ6 ) / r6

end subroutine LJ_setUp

!*****************************************************************************
subroutine evaluate_energy_LJ( nAtoms, a, b, c, position, energy, force, stress )
!*****************************************************************************
!
!  Project: MolecularDynamics
!
!  Created on: Wed 18 Apr 10:46:22 2018  by Eduardo R. Hernandez
!
!  Last Modified: Wed 23 May 12:22:05 2018
!
! ---
! Copyright (C) 2018       Eduardo R. Hernandez - ICMM
!  This file is distributed under the terms of the
!  GNU General Public License: see COPYING in the top directory
!  or http://www.gnu.org/copyleft/gpl.txt .
! ---
!
! Given cell vectors a, b, c and atomic positions, calculate energy, forces
! and stress according to the Lennard-Jones model.
!
!*****************************************************************************
!  modules used

!*****************************************************************************
!  No implicits please!
   
      implicit none

!*****************************************************************************
!  variables

      integer, intent(IN) :: nAtoms

      real (dp), dimension(3), intent (IN) :: a
      real (dp), dimension(3), intent (IN) :: b
      real (dp), dimension(3), intent (IN) :: c

      real (dp), dimension(:,:), intent (IN) :: position

      real (dp), intent (OUT) :: energy
      real (dp), dimension(:,:), intent (OUT) :: force
      real (dp), dimension(3,3), intent (OUT) :: stress

!*****************************************************************************
!  local variables

      integer :: i, j
      integer :: na, nb, nc
      integer :: n_a_cells, n_b_cells, n_c_cells

      real (dp) :: a_modulus, b_modulus, c_modulus
      real (dp) :: cosx, cosy, cosz
      real (dp) :: dV
      real (dp) :: raij, rbij, rcij
      real (dp) :: rxij, ryij, rzij
      real (dp) :: rij, rij2, rij6, rij7
      real (dp) :: xi, yi, zi, xj, yj, zj
      real (dp) :: volume

      real (dp), dimension(3) :: cosij
      real (dp), dimension(3) :: ra, rb, rc
      real (dp), allocatable, dimension(:,:) :: q
      real (dp), dimension(3) :: vec

!*****************************************************************************
!  start subroutine

      energy = zero
      force = zero
      stress = zero

! first calculate the reciprocal vectors of the cell; these are, but for 
! a factor of 2pi, the reciprocal lattice vectors

      vec(1) = b(2) * c(3) - c(2) * b(3)
      vec(2) = b(3) * c(1) - c(3) * b(1)
      vec(3) = b(1) * c(2) - c(1) * b(2)

      volume = dot_product( vec, a )

      ra = vec / volume

      vec(1) = c(2) * a(3) - a(2) * c(3)
      vec(2) = c(3) * a(1) - a(3) * c(1)
      vec(3) = c(1) * a(2) - a(1) * c(2)

      rb = vec / volume

      vec(1) = a(2) * b(3) - b(2) * a(3)
      vec(2) = a(3) * b(1) - b(3) * a(1)
      vec(3) = a(1) * b(2) - b(1) * a(2)

      rc = vec / volume

      a_modulus = sqrt( dot_product( a, a ) )
      b_modulus = sqrt( dot_product( b, b ) )
      c_modulus = sqrt( dot_product( c, c ) )

      n_a_cells = int( 2 * rcut / a_modulus ) + 1
      n_b_cells = int( 2 * rcut / b_modulus ) + 1
      n_c_cells = int( 2 * rcut / c_modulus ) + 1

! construct the array of positions in lattice representation

      allocate( q( 3, nAtoms ) )

      do i = 1, nAtoms
         q(1,i) = dot_product( ra, position(:,i) )
         q(2,i) = dot_product( rb, position(:,i) )
         q(3,i) = dot_product( rc, position(:,i) )
      end do

! now calculate away

      do i = 1, nAtoms

         xi = q(1,i) - floor( q(1,i) )
         yi = q(2,i) - floor( q(2,i) )
         zi = q(3,i) - floor( q(3,i) )

         do j = i, nAtoms

            xj = q(1,j) - floor( q(1,j) )
            yj = q(2,j) - floor( q(2,j) )
            zj = q(3,j) - floor( q(3,j) )

! we consider possible neighbours in neighbouring boxes

            do nc = -n_c_cells, n_c_cells
            do nb = -n_b_cells, n_b_cells
            do na = -n_a_cells, n_a_cells

               raij = xj + float( na ) - xi
               rbij = yj + float( nb ) - yi
               rcij = zj + float( nc ) - zi

               rxij = a(1) * raij + b(1) * rbij + c(1) * rcij
               ryij = a(2) * raij + b(2) * rbij + c(2) * rcij
               rzij = a(3) * raij + b(3) * rbij + c(3) * rcij

               rij2 = rxij * rxij + ryij * ryij + rzij * rzij

               if ( ( rij2 <= rcut2 ) .and. ( rij2 > zero ) ) then

                  rij = sqrt( rij2 )
                  rij6 = rij2 * rij2 * rij2
                  rij7 = rij6 * rij

                  cosx = rxij / rij
                  cosy = ryij / rij
                  cosz = rzij / rij

                  cosij = (/ cosx, cosy, cosz /) 

! accumulate contributions to energy, force and stress

                  energy = energy +                                    &
                      ( ALJ12 / rij6 - BLJ6 ) / rij6 - LJ_shift

                  dV = (-twelve * ALJ12 / rij6 + six * BLJ6 ) / rij7

                  force(1:3,i) = force(1:3,i) + dV * cosij

                  force(1:3,j) = force(1:3,j) - dV * cosij 

                  stress(1,1) = stress(1,1) + dV * cosx * cosx * rij
                  stress(2,2) = stress(2,2) + dV * cosy * cosy * rij
                  stress(3,3) = stress(3,3) + dV * cosz * cosz * rij

                  stress(1,2) = stress(1,2) + dV * cosx * cosy * rij
                  stress(1,3) = stress(1,3) + dV * cosx * cosz * rij
                  stress(2,3) = stress(2,3) + dV * cosy * cosz * rij

               end if

            end do
            end do
            end do

         end do ! end loop over j atoms
      end do ! end loop over i atoms

      stress(2,1) = stress(1,2)
      stress(3,1) = stress(1,3)
      stress(3,2) = stress(2,3)

      deallocate( q )

end subroutine evaluate_energy_LJ

end module LennardJones
