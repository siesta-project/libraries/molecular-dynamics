!*****************************************************************************
program example_PI_HO
!*****************************************************************************
!
!  Project: MolecularDynamics
!
!  Created on: Jan 2019 by Alfonso Gijón
!
! ---
! Copyright (C) 2018       Eduardo R. Hernandez - ICMM
!  This file is distributed under the terms of the
!  GNU General Public License: see COPYING in the top directory
!  or http://www.gnu.org/copyleft/gpl.txt .
! ---
!
! This means to serve as an example of a client program that runs a
! multireplica simulation using the MolecularDynamics module, combined with a
! simple Harmonic Oscillator potential.
!
! The whole ring polymer classical system follows a NVE dynamics
!
!*****************************************************************************
!  modules used

      use MD_State_module
      use MolecularDynamics
      use HarmonicOscillator

!*****************************************************************************
!  No implicits please!
   
      implicit none

!*****************************************************************************
!  local variables
      
      integer i, j, k, inext
      integer n, n0

      integer dyntype
      integer length
      integer energ
      integer n_atoms
      integer nSteps 
      integer nStep
      integer nThermostats
      integer nReplicas

      logical fixCellShape
      logical periodic
      logical start

      real (dp), dimension(:), allocatable :: tMass
      real (dp) :: bMass

      real (dp) :: gamma

      real (dp) :: alj, blj

      real (dp) :: delta_t
      real (dp) :: kinetic_energy
      real (dp) :: potential_energy
      real (dp) :: thermostat_energy
      real (dp) :: total_energy
      real (dp) :: temperature
      real (dp) :: temperature_target
      real (dp) :: beta,beta2, kT

      real (dp) :: xmax, vmax, xini, vini, rnum, initial_energy
      real (dp) :: koscillator, woscillator, moscillator

      real (dp) :: dalton_to_au, au_to_fs      

      real (dp) qKineticEnergy
      real (dp) qKineticEnergy_v2
      real (dp) qPotentialEnergy
      real (dp) qTotalEnergy
      real (dp) qTotalEnergy_v2

      real (dp) :: temperature_avrg, kinetic_avrg, potential_avrg
      real (dp) :: qkinetic_avrg, qpotential_avrg, qtotalenergy_avrg
      
      real (dp), dimension (3) :: a, b, c
      real (dp), allocatable, dimension (:,:) :: position0
      real (dp), allocatable, dimension (:) :: mass
      real (dp), dimension (3,3) :: stress

      character ( len = 30 ) :: restartFile
      character ( len = 30 ) :: newRestartFile

      type ( StateChain_type ) :: state_chain

      real (dp) :: tstart, tfinish, exetime

      integer, parameter :: nbin = 100
      real (dp), dimension (nbin) :: p_distribution
      real (dp) :: pmin, pmax, pl, pr, deltap
      real (dp) :: norm_p
      real (dp) :: pc, gaussian, sigma, px
      real (dp), allocatable, dimension (:,:) :: momentum
      type ( state_type ) :: statei
      character (len = 50) :: efile, pfile, str1, str2, str3, str4, str5, str6, str7

!*****************************************************************************
! begin program

      call cpu_time(tstart)

! simulation info
      
      restartFile = 'restartPI'
      newRestartFile = 'restartPI_2'

      nSteps = 100000

      dyntype = 4  ! Bussi-Parrinello dynamics for each degree of freedom
      
      start = .true.    ! this is not a re-start simulation
      periodic = .false. ! we assume periodic-boundary conditions

      fixCellShape = .false. ! we do not impose a fixed cell shape
      
      length = 0 ! length units of client are Bohr (atomic units)

      energ = 0 ! energy units are Hartree (atomic units)

      if ( length .eq. 0 ) then
         write(*,*) 'Length units are Bohr (atomic units)'
      else if ( length .eq. 1 ) then
         write(*,*) 'Length units are Angstrom (puaj!!!)' 
      else
         write(*,*) 'Unrecognised length units!'
         stop
      end if

      if ( energ .eq. 0 ) then
         write(*,*) 'Energy units are Hartree (atomic units)'
      else if ( energ .eq. 1 ) then
         write(*,*) 'Energy units are Rydberg (tutut!)' 
      else if ( energ .eq. 2 ) then
         write(*,*) 'Energy units are eV (puaj!!!!)' 
      else
         write(*,*) 'Unrecognised energy units!'
         stop
      end if

      nThermostats = 0
      !tMass = 1.0d0
      bMass = 1.0d0
      gamma = 0.05d0
      !gamma = 100.0d0

      ! parameteros of a hoscillator in a quantum regime, hw >> k_B * T
      
      !temperature_target = one / boltzmann_k
      !beta = one / ( boltzmann_k * temperature_target )
      beta = 15.8d0
      temperature_target = one / ( beta * boltzmann_k )
      
      beta2 = beta*beta
      kT = one / beta
      initial_energy = three * kT ! in Hartrees

      print*, 'hola'
      call setUnits( Bohr, Hartree )
      dalton_to_au = 1822.8885302342505d0
      au_to_fs = 0.02418884254d0

      delta_t = 0.05d0 * au_to_fs ! comvert time step from a.u. to fs

      a = (/60.55d0, 0.0d0, 0.0d0/)
      b = (/0.0d0, 60.55d0, 0.0d0/)
      c = (/0.0d0, 0.0d0, 60.55d0/)

      if ( length .eq. 1 ) then ! cell parameters and atomic positions 
                                ! must be in client units (Angstrom in this case)
         a = a / Angstrom
         b = b / Angstrom
         c = c / Angstrom

      end if

      n_atoms = 1

      write(6,*) 'n_atoms = ', n_atoms
      
      allocate( mass( n_atoms ) )
      allocate( position0( 3, n_atoms ) )

      
      ! oscillator parameters

      !woscillator = one ! in atomic units
      !moscillator = one ! in a.u.
      woscillator = one
      moscillator = 0.03d0 / woscillator
      
      koscillator = moscillator * woscillator * woscillator ! in a.u.
      mass = moscillator / dalton_to_au ! in daltons
      
! state info
      
      !open( unit = 5, file = 'coords.dat' )
      !do j = 1, n_atoms
      !    read(5,*) position0(1:3,j) 
      !    mass(j) = 39.631635050750646d0
      !end do

      ! all oscillators are initialised to the same position

      position0(1,1) = dsqrt( two * ( initial_energy / two) / koscillator ) ! in a.u.
      position0(2,1) = zero
      position0(3,1) = zero

      ! now create a chain of replicas (with same positions in each replica but random momenta)

      nReplicas = 200
      
      call setupStateChain( state_chain, nReplicas, temperature_target, n_atoms, &
           nThermostats, tMass, a, b, c, mass, position0, lattice = .false. )
      
      !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
      ! Modify to implement restart with multireplica simulations
      !if ( start ) then 
           !call setupStateChain( state_chain, nReplicas, temperature_target, n_atoms, &
           !nThermostats, tMass, a, b, c, mass, position0 )
      !else 
      !   call readRestart( state, restartFile, n0 )
      !end if
      !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!      
      
! set-up the model to be used

      !if ( ( energ .eq. 0 ) .and. ( length .eq. 0 ) ) then
      !  alj = 7644167.8958719987d0
      !  blj = 107.70436055866918d0
      !else if ( ( energ .eq. 1 ) .and. ( length .eq. 0 ) ) then
      !  alj = 7644167.8958719987d0 * two
      !  blj = 107.70436055866918d0 * two
      !else if ( ( energ .eq. 2 ) .and. ( length .eq. 1 ) ) then
      !  alj = 7644167.8958719987d0 / ( Ang**12 ) / eV 
      !  blj = 107.70436055866918d0 / ( Ang**6 ) / eV
      !end if
      
      !call LJ_setUp( alj, blj ) ! atomic units

      call HO_setUp( koscillator )
      
! compute initial forces

      call computeForces( state_chain, temperature_target )
      
! before entering the MD loop, set-up MD

      call MDChain_setup( state_chain, dyntype, delta_t, temperature_target, friction = gamma )

      print*, ''
      
      print*, 'step 0'
      call inquireStateChain(state_chain, temperature = temperature, &
           kineticEnergy = kinetic_energy, potentialEnergy = potential_energy)

      total_energy = kinetic_energy + potential_energy
      
      write(6,'(i5,f17.7,4f17.7)') 0, temperature, kinetic_energy, potential_energy, &
           total_energy

      print*, ''

      n0 = 0

      ! initialise momentum distributions
      p_distribution = zero
      !pmax = 3.0d0
      pmax = 0.2d0
      pmin = -pmax
      deltap = (pmax-pmin)/dfloat(nbin)

      allocate( momentum( 3, n_atoms ) )

      ! initialise averages
      temperature_avrg = zero
      kinetic_avrg = zero
      potential_avrg = zero
      qtotalenergy_avrg = zero

      ! virial estimator of total energy
      str1 = "qTotalEnergy-"
      write(str2,*) nReplicas
      str2 = adjustl(str2)
      str3 = "replicas-"
      write(str4,'(f17.2)') gamma
      str4 = adjustl(str4)
      str5 = "gamma.dat"
      efile = trim(str1)//trim(str2)//trim(str3)//trim(str4)//trim(str5)

      open(unit=23, file=efile)
      
      do n = 1, nSteps

         nStep = n0 + n

         ! advances positions of every replica by a time step, and momenta by half a time step

         call BussiParrinello_A( state_chain )
         !call velocityVerlet_StateChain_A( state_chain )

         ! update forces

         call computeForces (state_chain, temperature_target)

         ! with the new forces, uptade the remaining half a time step of momenta for every replica

         call BussiParrinello_B( state_chain )
         !call velocityVerlet_StateChain_B( state_chain )

         ! output info
         
         call inquireStateChain(state_chain, temperature = temperature, &
              kineticEnergy = kinetic_energy, potentialEnergy = potential_energy, &
              qKineticEnergy = qKineticEnergy, qPotentialEnergy = qPotentialEnergy, &
              qKineticEnergy_v2 = qKineticEnergy_v2)

         total_energy = kinetic_energy + potential_energy
         qTotalEnergy = qKineticEnergy + qPotentialEnergy
         qTotalEnergy_v2 = qKineticEnergy_v2 + qPotentialEnergy

         if ( mod(nStep, 10000) .eq. 0 ) then
            
            write(6,'(i7,f17.7,9f17.7)') nStep, temperature, kinetic_energy, potential_energy, &
                 total_energy, qKineticEnergy, qKineticEnergy_v2, qPotentialEnergy, &
                 qTotalEnergy, qTotalEnergy_v2

         end if
            
         temperature_avrg = temperature_avrg + temperature
         kinetic_avrg = kinetic_avrg + kinetic_energy
         potential_avrg = potential_avrg + potential_energy
         qtotalenergy_avrg = qtotalenergy_avrg + qTotalEnergy_v2 

         if ( mod(nStep, 1000) .eq. 0 ) then
            write(23,*) nstep, qtotalenergy_avrg / dfloat(nstep)
         end if

         ! compute px distribution of atoms

         do i = 1, nReplicas

            call getStateChain( state_chain, iReplica = i, state = statei )
            call inquire( statei, momentum = momentum )
            
            do k = 1, n_atoms

               px = momentum(1,k)

               do j = 0, nbin-1

                  pl = pmin + dfloat(j)*deltap
                  pr = pmin + dfloat(j+1)*deltap

                  if ( pl .lt. px .and. px .lt. pr) then
                     p_distribution(j+1) = p_distribution(j+1) + 1
                  end if
                  
               end do

            end do

         end do
         ! end of px distribution

      end do

      close(23)
      
      ! norm of momentum distribution
      norm_p = zero
      do j = 1, nbin
         norm_p = norm_p + p_distribution(j)*deltap
      enddo
      p_distribution = p_distribution / norm_p

      ! output distribution

      str1 = "pdistro-PI_HO-"
      write(str2,*) nReplicas
      str2 = adjustl(str2)
      str3 = "replicas-"
      write(str4,*) int(gamma)
      str4 = adjustl(str4)
      str5 = "gamma.dat"
      pfile = trim(str1)//trim(str2)//trim(str3)//trim(str4)//trim(str5)
      print*, pfile
      print*, efile
      
      open(unit=22, file=pfile)

      sigma = dsqrt( boltzmann_k * temperature_target * mass(1)*1822.89d0 )
      
      do j = 1, nbin
         pc = pmin + (dfloat(j)-half)*deltap

         gaussian = one / dsqrt( two * pi ) / sigma * dexp( -pc*pc /two /sigma**two )

         write(22,*) pc, p_distribution(j), gaussian
         
      end do
      close(22)

      temperature_avrg = temperature_avrg / dfloat(nSteps)
      kinetic_avrg = kinetic_avrg / dfloat(nSteps)
      potential_avrg = potential_avrg / dfloat(nSteps)
      write(*,*)
      write(*,*) 'Temperature average:', temperature_avrg / temperature_target
      write(*,*) 'Kinetic energy average:', kinetic_avrg / ( three / two * kT ) / dfloat(nReplicas)
      write(*,*) 'Potential energy average:', potential_avrg / ( three / two * kT ) / &
           dfloat(nReplicas)
      
      !call writeRestart( state, restartFile, nStep )      
      
      deallocate( mass )
      deallocate( position0 )

      call deleteStateChain( state_chain )

! end program

      call cpu_time(tfinish)
      exetime = tfinish - tstart
      write(*,*)
      write(*,*) 'Execution time (seconds, minutes, hours):'
      write(*,'(3f6.3)') exetime, exetime/60.0d0, exetime/3600.0d0

contains
  
!*****************************************************************************
subroutine computeForces( state_chain, temperature_target )
!*****************************************************************************
!
!  Project: MolecularDynamics
!
!  Created on: March 2019 by Alfonso Gijón
!
! ---
! Copyright (C) 2018       Eduardo R. Hernandez - ICMM
!  This file is distributed under the terms of the
!  GNU General Public License: see COPYING in the top directory
!  or http://www.gnu.org/copyleft/gpl.txt .
! ---
!
! This subroutine receives a chain of states to compute intra and inter-replica
! energies and forces, and update them.
!
!*****************************************************************************
!  modules used

!*****************************************************************************
!  No implicits please!
   
      implicit none

!*****************************************************************************
!  arguments

      type (StateChain_type), intent (INOUT) :: state_chain

      real (dp), intent(IN) :: temperature_target

!*****************************************************************************
!  local variables

      integer i, j, inext

      integer nAtoms
      integer nReplicas

      real (dp) :: beta, beta2

      real (dp) potential_energy
      real (dp) qPotentialEnergy
      real (dp) qKineticEnergy
      real (dp) qKineticEnergy_v2
      
      real (dp) vintra, vintra_total
      real (dp) vspring, vspring_total
      real (dp) vspring_v2, vspring_total_v2
      real (dp) :: kfactor, kspring
      
      real (dp), dimension (3) :: rspring
      real (dp), dimension (3) :: fspring
      real (dp), dimension (3) :: deltar

      real (dp), dimension (3) :: a, b, c
      real (dp), allocatable, dimension (:,:,:) :: position
      real (dp), allocatable, dimension (:,:,:) :: force
      real (dp), allocatable, dimension (:,:) :: centroid
      real (dp), allocatable, dimension (:) :: mass
      real (dp), dimension (3,3) :: stress      
      
      type (state_type), allocatable, dimension(:) :: state

!*****************************************************************************
!  start subroutine

      call getStateChain( state_chain, nReplicas = nReplicas, nAtoms = nAtoms )
      
      allocate( position( 3, nAtoms, nReplicas ) )
      allocate( force( 3, nAtoms, nReplicas ) )
      allocate( mass( nAtoms ) )
      allocate( state( nReplicas) )

      allocate( centroid( 3, nAtoms ) )
      centroid = zero

      call getStateChain( state_chain, mass = mass )
      
      ! compute intra-replica energy and forces
      
      vintra_total = zero
      
      do i = 1, nReplicas

         ! get the states from the chain
         call createState( state(i), nAtoms )
         call getStateChain( state_chain, iReplica = i, state = state(i) )

         ! get current configuration of each state and evaluate energy
         call getConfiguration( state(i), nAtoms, a, b, c, position(:,:,i) )
         call evaluate_energy_HO( nAtoms, position(:,:,i), vintra, &
              force(:,:,i) )

         vintra_total = vintra_total + vintra

         ! centroid position averaging over beads

         centroid(:,:) = centroid(:,:) + position(:,:,i)
         
      end do

      centroid = centroid / dfloat(nReplicas)

      ! Calculate vspring, using the internal forces and the path integral virial theorem

      vspring_total_v2 = zero

      do i = 1, nReplicas

         vspring_v2 = zero

         do j = 1, nAtoms

            !deltar = position(:,j,i) - position(:,j,1)
            deltar = position(:,j,i) - centroid(:,j)
            vspring_v2 = vspring_v2 + dot_product( deltar, force(:,j,i) )
            
         end do

         vspring_total_v2 = vspring_total_v2 + vspring_v2
         
      end do

      vspring_total_v2 = vspring_total_v2 * half / dfloat(nReplicas)

      ! In the the polymer Hamiltonian, the internal potential
      ! is reduced by a 1/nReplicas factor
      
      vintra_total = vintra_total / dfloat(nReplicas)
      force = force / dfloat(nReplicas)
      
      ! add inter-replica energy and forces (in a.u.)

      beta = one / ( boltzmann_k * temperature_target )
      beta2 = beta*beta

      kfactor = dfloat(nReplicas) / beta2

      vspring_total = zero

      do i = 1, nReplicas

         inext = i + 1
         if (inext == nReplicas+1) inext = 1

         vspring = zero

         do j = 1, nAtoms

            kspring = mass(j) * kfactor ! it depends on mass, number of replicas and temperature
            rspring = position(:,j,inext) - position(:,j,i)
            vspring = vspring + half * kspring * dot_product( rspring, rspring )
            
            fspring = kspring * rspring ! factor 2 is compensated with factor 1/2
            force(:,j,i) = force(:,j,i) + fspring
            force(:,j,inext) = force(:,j,inext) - fspring
                  
         end do

         vspring_total = vspring_total + vspring

      end do

      do i = 1, nReplicas
         call setForce( state(i), vintra_total, force(:,:,i), stress )
         call setStateCHain( state_chain, iReplica = i, state = state(i) )
      end do

      ! update classical potential energy of the chain

      potential_energy = vintra_total + vspring_total
      
      call setStateChain( state_chain, potentialEnergy = potential_energy )
      
      ! update quantum energies of the chain

      qPotentialEnergy = vintra_total
      qKineticEnergy = three * half * dfloat(nReplicas*nAtoms) / beta - vspring_total
      qKineticEnergy_v2 = three * half * dfloat(nAtoms) / beta - vspring_total_v2
      
      call setStateChain( state_chain, qPotentialEnergy = qPotentialEnergy, &
           qKineticEnergy = qKineticEnergy, qKineticEnergy_v2 = qKineticEnergy_v2 )

      deallocate( position )
      deallocate( force )
      deallocate( centroid )
      deallocate( mass )
      deallocate( state )
      
end subroutine computeForces    

end program example_PI_HO
