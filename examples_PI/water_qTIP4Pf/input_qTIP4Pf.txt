# logfile
'logfile.dat'
# Number of timesteps
10000
# Number of outputs in logfile
1000
# Frequency to compute spatial density  
20
# Restart label (1 if restart, 0 if not)
0
# Length of lattice vectors
60.55 60.55 60.55
# Number of water molecules of the cluster
8
# Number of replicas
120
# Friction parameter of Langevin dynamics (NVE dynamics if zero)
0.001
# Target temperature (K)
50.0
# Timestep (fs)
1.0
# Structures file
'structures-qTIP4Pf.dat'
