# logfile
'logfile.dat'
# Number of timesteps
200
# Number of outputs in logfile
200
# Frequency to compute spatial density  
1
# Restart label (1 if restart, 0 if not)
0
# Length of lattice vectors
60.55 60.55 60.55
# Number of water molecules of the cluster
100
# Number of replicas
1
# Friction parameter of Langevin dynamics (NVE dynamics if zero)
0.0
# Target temperature (K)
50.0
# Timestep (fs)
1.0
# xyz file
'./structures-SPC/100.xyz'
