!*****************************************************************************
program example_NVT
!*****************************************************************************
!
!  Project: MolecularDynamics
!
!  Created on: Thu 12 Apr 15:15:48 2018  by Eduardo R. Hernandez
!
!  Last Modified: Wed Apr  7 12:22:43 2021
!
! ---
! Copyright (C) 2018       Eduardo R. Hernandez - ICMM
!  This file is distributed under the terms of the
!  GNU General Public License: see COPYING in the top directory
!  or http://www.gnu.org/copyleft/gpl.txt .
! ---
!
! This means to serve as an example of a client program that runs an MD
! simulation using the MolecularDynamics module, combined with a simple
! Lennard-Jones potential.
!
!*****************************************************************************
!  modules used

      use LennardJones
      use MD_State_module
      use MolecularDynamics

!*****************************************************************************
!  No implicits please!
   
      implicit none

!*****************************************************************************
!  local variables

      integer i
      integer n, n0

      integer dyntype
      integer n_atoms
      integer nSteps 
      integer iStep
      integer nThermostats

      logical fixCellShape
      logical periodic
      logical start

      real (dp), dimension(:), allocatable :: tMass 
      real (dp) :: bMass 

      real(dp) :: alj, blj

      real (dp) :: delta_t
      real (dp) :: kinetic_energy
      real (dp) :: potential_energy
      real (dp) :: thermostat_energy
      real (dp) :: barostat_energy
      real (dp) :: total_energy
      real (dp) :: constantOfMotion
      real (dp) :: temperature

      real (dp), dimension (3) :: a, b, c
      real (dp), allocatable, dimension (:,:) :: position
      real (dp), allocatable, dimension (:,:) :: force
      real (dp), allocatable, dimension (:,:) :: velocity
      real (dp), allocatable, dimension (:,:) :: momentum
      real (dp), allocatable, dimension (:) :: mass
      real (dp), dimension (3,3) :: stress

      real (dp), dimension (:), allocatable :: thermostatKinetic

      character ( len = 30 ) :: restartFile
      character ( len = 30 ) :: newRestartFile

      type ( state_type ) :: state
      type ( thermostat_type ), dimension (:), allocatable :: thermostats

!*****************************************************************************
!  begin program

      restartFile = 'restartNVT_2'
      newRestartFile = 'restartNVT_3'

      nSteps = 10000

      dyntype = 1  ! NVT velocity-Verlet dynamics
      
      start = .false.    ! this is not a re-start simulation
      periodic = .true. ! we assume periodic-boundary conditions

      fixCellShape = .false. ! we do not impose a fixed cell shape

      nThermostats = 2
      allocate( tMass( nThermostats ) )
      tMass = 1000.0d0
      ! tMass = 830.60157586415589d0
      ! tMass = 39631.635050750646d0
      bMass = 1.0d0
      allocate( thermostats( nThermostats ) )
      allocate( thermostatKinetic( nThermostats ) )

      temperature = 1000.0d0

      call setUnits( Bohr, Hartree )

      delta_t = 1.0d0 ! 1 fs

      a = (/60.55d0, 0.0d0, 0.0d0/)
      b = (/0.0d0, 60.55d0, 0.0d0/)
      c = (/0.0d0, 0.0d0, 60.55d0/)

      n_atoms = 500

      allocate( mass( n_atoms ) )
      allocate( position( 3, n_atoms ) )
      allocate( force( 3, n_atoms ) )
      allocate( velocity( 3, n_atoms ) )
      allocate( momentum( 3, n_atoms ) )

      open( unit = 5, file = 'coords.dat' )
      do i = 1, n_atoms
          read(5,*) position(1:3,i) 
          mass(i) = 39.631635050750646d0
      end do

      open( unit = 5, file = 'vels.dat' )
      do i = 1, n_atoms
          read(5,*) velocity(1:3,i) 
      end do
      close( unit = 5 )

      ! create a state

      call createState( state, n_atoms,                                &
                        nThermostats = nThermostats, thermostatMass = tMass )

      if ( start ) then 

         call initialiseState( state, a, b, c, mass, position,  &
                        temperature = temperature, lattice = .true. )

      else 

         call readRestart( state, restartFile, n0 )

      end if

      ! set-up the model to be used

!     if ( ( energ .eq. 0 ) .and. ( length .eq. 0 ) ) then
        alj = 7644167.8958719987d0
        blj = 107.70436055866918d0
!     else if ( ( energ .eq. 1 ) .and. ( length .eq. 0 ) ) then
!       alj = 7644167.8958719987d0 * two
!       blj = 107.70436055866918d0 * two
!     else if ( ( energ .eq. 2 ) .and. ( length .eq. 1 ) ) then
!       alj = 7644167.8958719987d0 / ( Angstrom**12 ) / eV 
!       blj = 107.70436055866918d0 / ( Angstrom**6 ) / eV
!     end if

      call LJ_setUp( alj, blj ) ! atomic units

      ! before we can call evaluate_energy, we must get the current configuration from state

      call getConfiguration( state, n_atoms, a, b, c, position )
      call evaluate_energy_LJ( n_atoms, a, b, c, position, potential_energy, force, stress )
      call setForce( state, potential_energy, force, stress )
      
      ! before entering the MD loop, set-up MD

      call MD_setUp( state, dyntype, delta_t, temperature )

      open( unit = 8, file = 'ThermostatsNVTLiquid.dat' )
      open( unit = 11, file = 'NVTMomentaLiquid.dat')

      do i = 1, nSteps

         iStep = n0 + i

         call MD_stepA( state )
         call getConfiguration( state, n_atoms, a, b, c, position )
         call evaluate_energy_LJ( n_atoms, a, b, c, position, potential_energy, force, stress )
         call setForce( state, potential_energy, force, stress )
         call MD_stepB( state )

         !if ( mod( i, 10 ) .eq. 0 ) then
         if ( mod( i, 1 ) .eq. 0 ) then

            call inquire( state, temperature = temperature, kineticEnergy = kinetic_energy,   &
                          potentialEnergy = potential_energy, thermostatEnergy = thermostat_energy, &
                          momentum = momentum,  &
                          constantOfMotion = constantOfMotion, thermostat = thermostats )

            write(6,'(i5,f17.7,4f17.7)') iStep, temperature, kinetic_energy, potential_energy, thermostat_energy, constantOfMotion

            do n = 1, nThermostats
  
               thermostatKinetic(n) = thermostats(n) % momentum * thermostats(n) % momentum / thermostats(n) % mass

            end do

            write(8,'(f17.7)') thermostatKinetic

            write(11,'(i10)') iStep
            do n=1, n_atoms
               write(11,'(3f17.10)') momentum(1:3,n)
            end do
            write(11,*)

         end if

         if ( mod( i, 100 ) .eq. 0 ) call writeRestart( state, newRestartFile, iStep )

      end do

      close( unit = 8 )
      close( unit = 9 )

      call writeRestart( state, restartFile, iStep )

      deallocate( mass )
      deallocate( position )
      deallocate( force )
      deallocate( velocity )
      deallocate( momentum )
      deallocate( tMass )
      deallocate( thermostats )
      deallocate( thermostatKinetic )

      call deleteState( state )

end program example_NVT

