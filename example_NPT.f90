!*****************************************************************************
program example_NPT 
!*****************************************************************************
!
!  Project: MolecularDynamics    
!
!  Created on: Wed 16 May 10:51:22 2018  by Eduardo R. Hernandez
!
!  Last Modified: Wed Apr  7 12:24:24 2021
!
! ---
! Copyright (C) 2018       Eduardo R. Hernandez - ICMM
!  This file is distributed under the terms of the
!  GNU General Public License: see COPYING in the top directory
!  or http://www.gnu.org/copyleft/gpl.txt .
! ---
!
! This means to serve as an example of a client program that runs an MD
! simulation using the MolecularDynamics module, combined with a simple
! Lennard-Jones potential in the NPT ensemble.
!
!*****************************************************************************
!  modules used

      use LennardJones
      use MD_State_module
      use MolecularDynamics

!*****************************************************************************
!  No implicits please!
   
      implicit none

!*****************************************************************************
!  local variables

      integer i
      integer n0

      integer dyntype
      integer n_atoms
      integer nSteps 
      integer iStep
      integer nThermostats

      logical periodic
      logical fixCellShape
      logical start

      real (dp), dimension(:), allocatable :: tMass 
      real (dp) :: bMass 

      real(dp) :: alj, blj

      real (dp) :: delta_t
      real (dp) :: kinetic_energy
      real (dp) :: potential_energy
      real (dp) :: thermostat_energy
      real (dp) :: barostat_energy
      real (dp) :: total_energy
      real (dp) :: constantOfMotion
      real (dp) :: temperature
      real (dp) :: externalPressure, internalPressure

      real (dp), dimension (3) :: a, b, c
      real (dp), allocatable, dimension (:,:) :: position
      real (dp), allocatable, dimension (:,:) :: force
      real (dp), allocatable, dimension (:,:) :: velocity
      real (dp), allocatable, dimension (:) :: mass
      real (dp), dimension (3,3) :: externalStress, stress

      character ( len = 30 ) :: restartFile
      character ( len = 30 ) :: newRestartFile

      type ( state_type ) :: state
      type ( lattice_type ) :: Cell
      type ( thermostat_type ), allocatable, dimension (:) :: thermostats

!*****************************************************************************
!  begin program

      restartFile = 'restartNPT_2'
      newRestartFile = 'restartNPT_3'

      nSteps = 10000

      dyntype = 3  ! NVE velocity-Verlet dynamics
      
      start = .true.    ! this is a start simulation (set to false for restart)
      periodic = .true. ! we assume periodic-boundary conditions

      fixCellShape = .true. ! we do not impose a fixed cell shape

      call setUnits( Bohr, Hartree )

      nThermostats = 1
      allocate( tMass( nThermostats ) )
      tMass = 1000.0d0
      bMass = 1.0d-6

      temperature = 1000.0d0   ! in K
      externalPressure = 0.2d0 ! if press = 0 in kB
      externalStress = 0.0d0   ! if press = 1 in GPa

      delta_t = 1.0d0 ! 1 fs

      a = (/60.55, 0.0, 0.0/)
      b = (/0.0, 60.55, 0.0/)
      c = (/0.0, 0.0, 60.55/)

      n_atoms = 500

      allocate( mass( n_atoms ) )
      allocate( position( 3, n_atoms ) )
      allocate( force( 3, n_atoms ) )
      allocate( velocity( 3, n_atoms ) )

      open( unit = 5, file = 'coords.dat' )
      do i = 1, n_atoms
          read(5,*) position(1:3,i) 
          mass(i) = 39.94751148493173d0
      end do
      close( unit = 5 )

      open( unit = 5, file = 'vels.dat' )
      do i = 1, n_atoms
          read(5,*) velocity(1:3,i) 
      end do
      close( unit = 5 )

      call createState( state, n_atoms, nThermostats, thermostatMass = tMass,         &
                        barostatMass = bMass )

      if ( start ) then 

         call initialiseState( state, a, b, c, mass, position,  &
                        temperature = temperature, lattice = .true., periodic = periodic )

      else 

         call readRestart( state, restartFile, n0 )

      end if

      ! set-up the model to be used

!     if ( ( energ .eq. 0 ) .and. ( length .eq. 0 ) ) then
        alj = 7644167.8958719987d0
        blj = 107.70436055866918d0
!     else if ( ( energ .eq. 1 ) .and. ( length .eq. 0 ) ) then
!       alj = 7644167.8958719987d0 * two
!       blj = 107.70436055866918d0 * two
!     else if ( ( energ .eq. 2 ) .and. ( length .eq. 1 ) ) then
!       alj = 7644167.8958719987d0 / ( Ang**12 ) / eV 
!       blj = 107.70436055866918d0 / ( Ang**6 ) / eV
!     end if

      call LJ_setUp( alj, blj ) ! atomic units

      call inquire( state, Cell = Cell )

      write(*,*) 'Cell % a = ', a(1:3), Cell % a_modulus
      write(*,*) 'Cell % b = ', b(1:3), Cell % b_modulus
      write(*,*) 'Cell % c = ', c(1:3), Cell % c_modulus

      write(*,*) 'Cell % alpha = ', Cell % alpha
      write(*,*) 'Cell % beta = ', Cell % beta
      write(*,*) 'Cell % gamma = ', Cell % gamma

      write(*,*) 'Cell % volume = ', Cell % volume

      ! before we can call evaluate_energy, we must get the current configuration from state

      call getConfiguration( state, n_atoms, a, b, c, position )
      call evaluate_energy_LJ( n_atoms, a, b, c, position, potential_energy, force, stress )
      ! then accumulate forces and stress into state
      call setForce( state, potential_energy, force, stress )

      ! before entering the MD loop, set-up the MD simulation

      call MD_setUp( state, dyntype, delta_t, temperature, externalPressure, externalStress, fixCellShape )

      open(unit=7,file='volume.dat')

      do i = 1, nSteps

         iStep = n0 + i

         call MD_stepA( state )
         call getConfiguration( state, n_atoms, a, b, c, position )
         call evaluate_energy_LJ( n_atoms, a, b, c, position, potential_energy, force, stress )
         call setForce( state, potential_energy, force, stress )
         call MD_stepB( state )

         if ( mod( i, 10 ) .eq. 0 ) then

             call inquire( state, temperature = temperature, kineticEnergy = kinetic_energy,         &
                           potentialEnergy = potential_energy, thermostatEnergy = thermostat_energy, &
                           barostatEnergy = barostat_energy, constantOfMotion = constantOfMotion,    &
                           pressure = internalPressure, Cell = Cell )

            write(6,'(i5,f17.7,5f17.7)') iStep, temperature, kinetic_energy, potential_energy, thermostat_energy, &
                           barostat_energy, constantOfMotion

            write(7,'(5f17.7)') internalPressure, Cell % a_modulus, Cell % b_modulus, Cell % c_modulus, Cell % volume

         end if

         if ( mod( i, 100 ) .eq. 0 ) call writeRestart( state, newRestartFile, iStep )

      end do

      close( unit = 7 )

      allocate( thermostats( nThermostats ) )
      call inquire( state, thermostat = thermostats )
      do i = 1, nThermostats
          write(*,*) i, thermostats(i) % position, thermostats(i) % momentum
      end do

      call writeRestart( state, newRestartFile, iStep )

      deallocate( mass )
      deallocate( position )
      deallocate( force )
      deallocate( tMass )

      call deleteState( state )

end program example_NPT
