!*****************************************************************************
program example_PI_water_SPCF
!*****************************************************************************
!
!  Project: MolecularDynamics
!
!  Created on: Jan 2019 by Alfonso Gijón
!
! ---
! Copyright (C) 2018       Eduardo R. Hernandez - ICMM
!  This file is distributed under the terms of the
!  GNU General Public License: see COPYING in the top directory
!  or http://www.gnu.org/copyleft/gpl.txt .
! ---
!
! This means to serve as an example of a client program that runs a
! multireplica simulation using the Path Integral interpretation of quantum
! statistical mechanics. A Langevin dynamics is followed by each atom of
! of the extended system, to ensure that the NVT ensemble is correctly sampled
!
! The physical system is a cluster of water molecules, interacting via a SPCF
! potential. 
!
!*****************************************************************************
!  modules used

      use MD_State_module
      use MolecularDynamics
      use SPCF

      use omp_lib

!*****************************************************************************
!  No implicits please!
   
      implicit none

!*****************************************************************************
!  local variables
      
      integer i, j, k
      integer i1,i2
      integer n, n0
      integer nStep
      integer iat, imol, jmol, nmol
      integer iblock

      integer length
      integer energ
      integer dyntype
      integer nSteps
      integer n_atoms
      integer n_mols
      integer nReplicas
      
      integer istart
      integer noutputs, nperiod, nperiod_density

      integer proc_num, thread_num

      integer nThermostats
      real (dp), dimension(:), allocatable :: tMass
      
      logical fixCellShape
      logical periodic
      logical start

      real (dp), parameter :: ang_to_au = one / 0.529177d0
      real (dp), parameter :: kcalmol_to_au = one / 627.510d0

      real (dp) :: gamma
      real (dp) :: temperature_target
      real (dp) :: delta_t      

      real (dp) :: tstart_cpu, tfinish_cpu, cputime
      integer :: count_0, count_1, count_rate, count_max
      real (dp) :: walltime
      real (dp) :: tforces
      integer :: count0, count1
      real (dp) :: tpdistro
      integer :: countp0, countp1
      real (dp) :: twrite
      integer :: countw0, countw1
      real (dp) :: tcentroid
      integer :: countc0, countc1
      real (dp) :: tmd
      integer :: countmd0, countmd1
      real (dp) :: tb
      integer :: countb0, countb1
      real (dp) :: ta
      integer :: counta0, counta1
      real (dp) :: taux      
      
      real (dp) :: kinetic_energy
      real (dp) :: potential_energy
      real (dp) :: thermostat_energy
      real (dp) :: total_energy
      real (dp) :: temperature
      real (dp) :: beta, beta2, kT
      real (dp) :: qKineticEnergy
      real (dp) :: qKineticEnergy_v2
      real (dp) :: qPotentialEnergy
      real (dp) :: qTotalEnergy
      real (dp) :: qTotalEnergy_v2

      real (dp) :: temperature_avrg, kinetic_avrg, potential_avrg, totalenergy_avrg
      real (dp) :: qkinetic_avrg, qkinetic_v2_avrg, qpotential_avrg
      real (dp) :: qtotalenergy_avrg, qtotalenergy_v2_avrg
      real (dp) :: sigma_kinetic, sigma_potential, sigma_totalenergy, sigma_qtotalenergy_v2
      real (dp) :: sigma_qkinetic, sigma_qkinetic_v2, sigma_qpotential, sigma_qtotalenergy

      real (dp), allocatable, dimension(:) :: gyration2, gyration2_avrg
      real (dp) :: radius2, radius2_avrg, radius0
      real (dp) :: gyration2_o, gyration2_h
      real (dp) :: sigma_radius2, sigma_gyration2_o, sigma_gyration2_h
      real (dp) :: lindemann, lindemann_avrg
      real (dp) :: sigma_lindemann

      integer, parameter :: nblocks = 100
      real (dp), dimension (nblocks) :: kin_avrg, pot_avrg, tote_avrg, qkin_avrg
      real (dp), dimension (nblocks) :: qkin_v2_avrg, qpot_avrg, qtote_avrg, qtote_v2_avrg
      real (dp), dimension(nblocks) :: r2_avrg
      real (dp) :: dblock

      integer, parameter :: nbin = 100
      real (dp) :: pmin_o, pmax_o, pmin_h, pmax_h, deltap_o, deltap_h
      real (dp) :: pmin_h2o, pmax_h2o, deltap_h2o, mh2o, px_h2o
      real (dp) :: norm_ph, norm_po, norm_ph2o
      real (dp) :: pl, pr, pc, px, gaussian, sigma
      real (dp) :: sigma_o, sigma_h, sigma_h2o
      real (dp), dimension (nbin) :: pdistro_h      
      real (dp), dimension (nbin) :: pdistro_o
      real (dp), dimension (nbin) :: pdistro_h2o
      real (dp), allocatable, dimension (:,:) :: momentum

      integer, parameter :: nbinx = 100
      real (dp) :: x1c, x2c, xmax, xmin, dx
      real (dp), dimension(3,nbinx,nbinx) :: density_o
      real (dp), dimension(3,nbinx,nbinx) :: density_h
      real (dp), dimension(3,nbinx,nbinx) :: density_o_avrg      
      real (dp), dimension(3,nbinx,nbinx) :: density_h_avrg
      
      real (dp), dimension(3) :: a, b, c
      real (dp), dimension(3) :: rcm
      real (dp), allocatable, dimension (:,:) :: position0
      real (dp), allocatable, dimension (:,:) :: centroid        
      real (dp), allocatable, dimension (:,:) :: centroid_avrg
      real (dp), allocatable, dimension (:,:) :: rij
      real (dp), allocatable, dimension (:,:) :: rij2
      real (dp), allocatable, dimension (:,:) :: rij_avrg
      real (dp), allocatable, dimension (:,:) :: rij2_avrg          
      real (dp), allocatable, dimension (:,:,:) :: polymer
      real (dp), allocatable, dimension (:) :: mass

      type ( StateChain_type ) :: state_chain
      
      character ( len = 30 ) :: restartFile
      character ( len = 30 ) :: newRestartFile
      character ( len = 30 ) :: logfile, xyzfile
      character ( len = 30 ) :: idat
      logical exist

!*****************************************************************************
! begin program

      call cpu_time(tstart_cpu)
      call system_clock( count_0, count_rate, count_max )
      !walltime = omp_get_wtime()
      
! simulation info
      
      restartFile = 'restartPI'
      newRestartFile = 'restartPI_2'

      write(*,*) 'Reading input'
      write(*,*)

      read(5,*)
      read(5,*) logfile
      logfile = trim(logfile)
      write(*,*) '# logfile'
      write(*,*) logfile
      !logfile = "logfile.dat"
      inquire( file = logfile, exist = exist )
      if (exist) then
         open( unit = 20, file = logfile, status = "old", position = "append", action = "write")
      else
         open( unit = 20, file = logfile)
      end if
      
      read(5,*)
      read(5,*) nSteps
      write(*,*) '# Number of timesteps'
      write(*,*) nSteps      
      !nSteps = 100000

      read(5,*)
      read(5,*) noutputs
      write(*,*) '# Number of outputs in logfile'
      write(*,*) noutputs      
      !noutputs = 1000
      nperiod = nSteps / noutputs

      read(5,*)
      read(5,*) nperiod_density
      write(*,*) '# Frequency to compute spatial density'
      write(*,*) nperiod_density
      !nperiod_density = 1

      dyntype = 4  ! Bussi-Parrinello dynamics for each degree of freedom

      read(5,*)
      read(5,*) istart
      write(*,*) '# Restart label (1 if restart, 0 if not)'
      write(*,*) istart
      if (istart .eq. 1) then
         start = .false.
      else
         start = .true.
      end if
      !start = .true.    ! this is not a re-start simulation
      !start = .false.   ! restart simulation
      
      periodic = .false. ! we do not assume periodic-boundary conditions
      fixCellShape = .false. ! we do not impose a fixed cell shape
      
      length = 0 ! length units of client are Bohr (atomic units)
      energ = 0 ! energy units are Hartree (atomic units)
      call setUnits( Bohr, Hartree )

      a = zero
      b = zero
      c = zero
      read(5,*)
      read(5,*) a(1), b(2), c(3)
      write(*,*) '# Lattice vectors'
      write(*,*) a
      write(*,*) b
      write(*,*) c      
      !a = (/60.55_dp, 0.0_dp, 0.0_dp/)
      !b = (/0.0_dp, 60.55_dp, 0.0_dp/)
      !c = (/0.0_dp, 0.0_dp, 60.55_dp/)

      if ( length .eq. 1 ) then ! cell parameters and atomic positions 
                                ! must be in client units (Angstrom in this case)
         a = a / Angstrom
         b = b / Angstrom
         c = c / Angstrom

      end if

      nThermostats = 0
      !tMass = 1.0_dp

      read(5,*)
      read(5,*) n_mols
      write(*,*) '# Number of water molecules of the cluster'
      write(*,*) n_mols
      !n_mols = 2
      n_atoms = 3 * n_mols
      
      read(5,*)
      read(5,*) nReplicas
      write(*,*) '# Number of replicas'
      write(*,*) nReplicas
      !nReplicas = 120

      read(5,*)
      read(5,*) gamma
      write(*,*) '# Friction parameter of Langevin dynamics (NVE dynamics if zero)'
      write(*,*) gamma
      !gamma = 0.001_dp   ! friction parameter of Langevin dynamics
      !gamma = zero      ! if gamma = zero, NVE dynamics is followed

      read(5,*)
      read(5,*) temperature_target
      write(*,*) '# Target temperature (K)'
      write(*,*) temperature_target
      !temperature_target = 50.0_dp
      beta = one / ( boltzmann_k * temperature_target )
      beta2 = beta*beta
      kT = one / beta

      read(5,*)
      read(5,*) delta_t
      write(*,*) '# Timestep (fs)'
      write(*,*) delta_t
      !delta_t = 1.0_dp ! 1 fs

      ! write some information about units, number of atoms...
      
      if ( length .eq. 0 ) then
         write(20,*) 'Length units are Bohr (atomic units)'
      else if ( length .eq. 1 ) then
         write(20,*) 'Length units are Angstrom (puaj!!!)' 
      else
         write(20,*) 'Unrecognised length units!'
         stop
      end if

      if ( energ .eq. 0 ) then
         write(20,*) 'Energy units are Hartree (atomic units)'
      else if ( energ .eq. 1 ) then
         write(20,*) 'Energy units are Rydberg (tutut!)' 
      else if ( energ .eq. 2 ) then
         write(20,*) 'Energy units are eV (puaj!!!!)' 
      else
         write(20,*) 'Unrecognised energy units!'
         stop
      end if

      write(20,*) 'n_atoms = ', n_atoms

! allocate some arrays
      
      allocate( mass( n_atoms ) )
      allocate( position0( 3, n_atoms ) )
      allocate( momentum( 3, n_atoms ) )
      allocate( centroid( 3, n_atoms ) )
      allocate( centroid_avrg( 3, n_atoms ) )      
      allocate( polymer( 3, n_atoms, nReplicas ) )
      allocate( gyration2( n_atoms ) )
      allocate( gyration2_avrg( n_atoms ) )
      allocate( rij( n_mols, n_mols ) )
      allocate( rij2( n_mols, n_mols ) )
      allocate( rij_avrg( n_mols, n_mols ) )
      allocate( rij2_avrg( n_mols, n_mols ) )

! parallelization settings

      proc_num = omp_get_num_procs ( )
      thread_num = omp_get_max_threads ( )
      !call omp_set_num_threads(4)

      write(*,*)
      write(*,*) 'Number of processors and threads'
      write(*,*) proc_num, thread_num
      write(*,*)

      write(20,*)
      write(20,*) 'Number of processors and threads'
      write(20,*) proc_num, thread_num
      write(20,*)                        

! state info

      read(5,*)
      read(5,*) xyzfile
      xyzfile = trim(xyzfile)
      write(*,*) 'Structures file'
      write(*,*) xyzfile
      close(5)
      
      if ( start ) then

         ! read positions of atoms
      
         open( unit = 10, file = xyzfile )

         read(10,*)
         read(10,*)
         do imol = 1, n_mols
            do j = 1, 3
               iat = j + 3 * (imol-1)
               read(10,*) idat, position0(1:3,iat) 
               if ( j .eq. 1) then
                  mass(iat) = 15.999_dp ! oxygen mass
               else
                  mass(iat) = 1.00784_dp ! hydrogen mass
               end if
            end do
         end do
         
         close(10)

         ! Translate the center of masses of the cluster to the origin

         rcm = zero
         do imol = 1, n_mols
            do j = 1, 3
               iat = j + 3 * (imol-1)
               rcm = rcm + mass(iat) * position0(:,iat)
            end do
         end do
         rcm = rcm / sum( mass )
         do iat = 1, n_atoms
            position0(:,iat) = position0(:,iat) - rcm(:)
         end do
         position0 = position0 * ang_to_au ! convert Angstrom to atomic units

         ! now create a chain of replicas (with same positions in each replica but random momenta)
         
         call setupStateChain( state_chain, nReplicas, temperature_target, n_atoms, &
              nThermostats, tMass, a, b, c, mass, position0, periodic = periodic )
         n0 = 0

      else
         
         call readRestartChain( state_chain, restartFile, n0 )
         
      end if

! setup SPCF water potential and compute initial energy and forces

      call SPCF_setup()

      call computeForces( state_chain, temperature_target, gyration2, centroid )

! compute initial radius to set size of the spatial mesh
      
      call CentroidProperties( state_chain, centroid, radius2, rij, rij2 )

      radius0 = dsqrt(radius2)
      
! before entering the MD loop, set-up MD and compute initial classical energy

      call MDChain_setup( state_chain, dyntype, delta_t, temperature_target, friction = gamma )

      call inquireStateChain(state_chain, temperature = temperature, &
           kineticEnergy = kinetic_energy, potentialEnergy = potential_energy)

      total_energy = kinetic_energy + potential_energy

      write(*,*)
      write(*,*) 'INITIAL STEP'
      write(*,'(i10,f17.7,4f17.7)') n0, temperature, kinetic_energy, &
           potential_energy, total_energy
      write(*,*)
      
      write(20,*)
      write(20,*) 'INITIAL STEP'
      write(20,'(i10,f17.7,4f17.7)') n0, temperature, kinetic_energy, &
           potential_energy, total_energy
      write(20,*)

! initialise some quantities      
      
      ! initialise momentum distributions
      
      pdistro_o = zero
      pdistro_h = zero
      pdistro_h2o = zero
      sigma_o = dsqrt( boltzmann_k * temperature_target * mass(1)*1822.89_dp )
      sigma_h = dsqrt( boltzmann_k * temperature_target * mass(2)*1822.89_dp )      
      mh2o = ( mass(1) + two*mass(2) ) * 1822.89_dp
      sigma_h2o = dsqrt( boltzmann_k * temperature_target * mh2o )      
      pmax_o = three * sigma_o
      pmax_h = three * sigma_h
      pmax_h2o = three * sigma_h2o
      pmin_o = -pmax_o
      pmin_h = -pmax_h
      pmin_h2o = - pmax_h2o
      deltap_o = (pmax_o-pmin_o)/dfloat(nbin)
      deltap_h = (pmax_h-pmin_h)/dfloat(nbin)
      deltap_h2o = (pmax_h2o-pmin_h2o)/dfloat(nbin)

      ! initialise averages
      
      temperature_avrg = zero
      kinetic_avrg = zero
      potential_avrg = zero
      totalenergy_avrg = zero
      qtotalenergy_avrg = zero
      qtotalenergy_v2_avrg = zero      
      qkinetic_avrg = zero
      qkinetic_v2_avrg = zero      
      qpotential_avrg = zero

      kin_avrg = zero
      pot_avrg = zero
      tote_avrg = zero
      qkin_avrg = zero
      qkin_v2_avrg = zero
      qpot_avrg = zero
      qtote_avrg = zero
      qtote_v2_avrg = zero
      dblock = dfloat(nSteps)/dfloat(nblocks)

      radius2_avrg = zero
      gyration2_avrg = zero
      r2_avrg = zero
      centroid_avrg = zero
      rij_avrg = zero
      rij2_avrg = zero

      density_o_avrg = zero      
      density_h_avrg = zero

! launch dynamics
      tforces = zero
      tpdistro = zero
      twrite = zero
      tcentroid = zero
      tmd = zero
      tb = zero
      ta = zero

      do n = 1, nSteps

         nStep = n0 + n

         ! advances positions of every replica by a time step, and momenta by half a time step

         call BussiParrinello_A( state_chain )
         !call velocityVerlet_StateChain_A( state_chain )

         ! update forces

         call computeForces( state_chain, temperature_target, gyration2, centroid )

         ! with the new forces, uptade the remaining half a time step of momenta for every replica

         call BussiParrinello_B( state_chain )
         !call velocityVerlet_StateChain_B( state_chain )

         ! write dynamical information
         
         call inquireStateChain(state_chain, temperature = temperature, &
              kineticEnergy = kinetic_energy, potentialEnergy = potential_energy, &
              qKineticEnergy = qKineticEnergy, qPotentialEnergy = qPotentialEnergy, &
              qKineticEnergy_v2 = qKineticEnergy_v2)

         if ( mod(nStep, nperiod_density) .eq. 0 ) then
            call CentroidProperties( state_chain, centroid, radius2, rij, rij2, &
                 radius0 = radius0, density_o = density_o, density_h = density_h)
         else
            call CentroidProperties( state_chain, centroid, radius2, rij, rij2 )
         end if

         density_o_avrg = density_o_avrg + density_o
         density_h_avrg = density_h_avrg + density_h

         gyration2_avrg = gyration2_avrg + gyration2
         radius2_avrg = radius2_avrg + radius2
         centroid_avrg = centroid_avrg + centroid
         rij_avrg = rij_avrg + rij
         rij2_avrg = rij2_avrg + rij2         
         
         total_energy = kinetic_energy + potential_energy
         qTotalEnergy = qKineticEnergy + qPotentialEnergy
         qTotalEnergy_v2 = qKineticEnergy_v2 + qPotentialEnergy

         temperature_avrg = temperature_avrg + temperature
         kinetic_avrg = kinetic_avrg + kinetic_energy
         potential_avrg = potential_avrg + potential_energy
         totalenergy_avrg = totalenergy_avrg + total_energy
         qtotalenergy_avrg = qtotalenergy_avrg + qTotalEnergy
         qtotalenergy_v2_avrg = qtotalenergy_v2_avrg + qTotalEnergy_v2         
         qkinetic_avrg = qkinetic_avrg + qKineticEnergy
         qkinetic_v2_avrg = qkinetic_v2_avrg + qKineticEnergy_v2
         qpotential_avrg = qpotential_avrg + qPotentialEnergy

         iblock = int( n / dblock ) + 1
         kin_avrg(iblock) = kin_avrg(iblock) + kinetic_energy
         pot_avrg(iblock) = pot_avrg(iblock) + potential_energy
         tote_avrg(iblock) = tote_avrg(iblock) + total_energy
         qkin_avrg(iblock) = qkin_avrg(iblock) + qKineticEnergy
         qkin_v2_avrg(iblock) = qkin_v2_avrg(iblock) + qKineticEnergy_v2
         qpot_avrg(iblock) = qpot_avrg(iblock) + qPotentialEnergy
         qtote_avrg(iblock) = qtote_avrg(iblock) + qTotalEnergy
         qtote_v2_avrg(iblock) = qtote_v2_avrg(iblock) + qTotalEnergy_v2
         r2_avrg(iblock) = r2_avrg(iblock) + radius2

         if ( mod(nStep, nperiod) .eq. 0 ) then

            ! current values
            
            write(*,'(i10,f17.7,9f17.7)') nStep, temperature, kinetic_energy, potential_energy, &
                 total_energy, qKineticEnergy, qKineticEnergy_v2, qPotentialEnergy, &
                 qTotalEnergy, qTotalEnergy_v2

            ! accumulated averages
            
            write(20,'(i10,f17.7,9f17.7)') nStep, temperature_avrg / dfloat(n), &
                 kinetic_avrg / dfloat(n), potential_avrg / dfloat(n), &
                 totalenergy_avrg / dfloat(n), qkinetic_avrg / dfloat(n), &
                 qkinetic_v2_avrg / dfloat(n), qpotential_avrg / dfloat(n), &
                 qtotalenergy_avrg / dfloat(n), qtotalenergy_v2_avrg / dfloat(n)
         
         end if

         ! compute px distribution of atoms and molecules

         if (start) then

            !!$OMP PARALLEL &
            !!$OMP PRIVATE( i, momentum, imol, px_h2o, k, iat, px, j, pl, pr ) 
            !!$OMP DO
            do i = 1, nReplicas

               call getStateChain( state_chain, iReplica = i, momentum = momentum )

               do imol = 1, n_mols
                  
                  px_h2o = zero
                  
                  do k = 1, 3
                     
                     iat = k + 3*(imol-1)
                     px = momentum(1,iat)
                     px_h2o = px_h2o + px
                     
                     if ( k.eq.1 ) then ! oxygen atom
                        
                        do j = 0, nbin-1
                           pl = pmin_o + dfloat(j)*deltap_o
                           pr = pmin_o + dfloat(j+1)*deltap_o
                           if ( pl .lt. px .and. px .lt. pr) then
                              pdistro_o(j+1) = pdistro_o(j+1) + 1
                           end if
                        end do
                        
                     else              ! hydrogen atoms 

                        do j = 0, nbin-1
                           pl = pmin_h + dfloat(j)*deltap_h
                           pr = pmin_h + dfloat(j+1)*deltap_h
                           if ( pl .lt. px .and. px .lt. pr) then
                              pdistro_h(j+1) = pdistro_h(j+1) + 1
                           end if
                        end do
                  
                     end if

                  end do ! end of atoms inside a moleclue

                  do j = 0, nbin-1
                     pl = pmin_h2o + dfloat(j)*deltap_h2o
                     pr = pmin_h2o + dfloat(j+1)*deltap_h2o
                     if ( pl .lt. px_h2o .and. px_h2o .lt. pr) then
                        pdistro_h2o(j+1) = pdistro_h2o(j+1) + 1
                     end if
                  end do

                  px_h2o = zero
                  
               end do ! end of imol
               
            end do ! end of replicas
            !!$OMP END DO
            !!$OMP END PARALLEL
            

         end if ! end calculation of momentum distributions
         
         ! write restart file with some period
         
         if ( mod(nstep,200000) .eq. 0 ) then
            call writeRestartChain( state_chain, newrestartFile, nStep )
         end if

      end do ! end of dynamics
      
! normalize momenta distribution and averages
      
      norm_po = zero
      norm_ph = zero
      norm_ph2o = zero
      do j = 1, nbin
         norm_po = norm_po + pdistro_o(j)*deltap_o
         norm_ph = norm_ph + pdistro_h(j)*deltap_h
         norm_ph2o = norm_ph2o + pdistro_h2o(j)*deltap_h2o
      enddo
      pdistro_o = pdistro_o / norm_po
      pdistro_h = pdistro_h / norm_ph
      pdistro_h2o = pdistro_h2o / norm_ph2o

      gyration2_avrg = gyration2_avrg / dfloat(nSteps)
      radius2_avrg = radius2_avrg / dfloat(nSteps)
      centroid_avrg = centroid_avrg / dfloat(nSteps)
      rij_avrg = rij_avrg / dfloat(nSteps)
      rij2_avrg = rij2_avrg / dfloat(nSteps)      
      
      temperature_avrg = temperature_avrg / dfloat(nSteps)
      kinetic_avrg = kinetic_avrg / dfloat(nSteps)
      potential_avrg = potential_avrg / dfloat(nSteps)
      totalenergy_avrg = totalenergy_avrg / dfloat(nSteps)
      qtotalenergy_avrg = qtotalenergy_avrg / dfloat(nSteps)
      qtotalenergy_v2_avrg = qtotalenergy_v2_avrg / dfloat(nSteps)
      qkinetic_v2_avrg = qkinetic_v2_avrg / dfloat(nSteps)                  
      qkinetic_avrg = qkinetic_avrg / dfloat(nSteps)
      qpotential_avrg = qpotential_avrg / dfloat(nSteps)

      kin_avrg = kin_avrg / dblock
      pot_avrg = pot_avrg / dblock
      tote_avrg = tote_avrg / dblock
      qkin_avrg = qkin_avrg / dblock
      qkin_v2_avrg = qkin_v2_avrg / dblock
      qpot_avrg = qpot_avrg / dblock
      qtote_avrg = qtote_avrg / dblock
      qtote_v2_avrg = qtote_v2_avrg / dblock
      r2_avrg = r2_avrg / dblock

      !density_o_avrg = density_o_avrg / dfloat(nsteps/nperiod_density)
      !density_h_avrg = density_h_avrg / dfloat(nsteps/nperiod_density)

      do k = 1, 3
         density_o_avrg(k,:,:) = density_o_avrg(k,:,:) / sum( density_o_avrg(k,:,:) )
         density_h_avrg(k,:,:) = density_h_avrg(k,:,:) / sum( density_h_avrg(k,:,:) )      
      end do

! compute statistical errors

      sigma_kinetic = zero
      sigma_potential = zero
      sigma_totalenergy = zero
      sigma_qkinetic = zero
      sigma_qkinetic_v2 = zero
      sigma_qpotential = zero
      sigma_qtotalenergy = zero
      sigma_qtotalenergy_v2 = zero
      sigma_radius2 = zero
      do iblock = 1, nblocks
         sigma_kinetic = sigma_kinetic + ( kin_avrg(iblock) - kinetic_avrg )**two
         sigma_potential = sigma_potential + ( pot_avrg(iblock) - potential_avrg )**two
         sigma_totalenergy = sigma_totalenergy + ( tote_avrg(iblock) - totalenergy_avrg )**two
         sigma_qkinetic = sigma_qkinetic + ( qkin_avrg(iblock) - qkinetic_avrg )**two
         sigma_qkinetic_v2 = sigma_qkinetic_v2 + ( qkin_v2_avrg(iblock) - qkinetic_v2_avrg )**two
         sigma_qpotential = sigma_qpotential + ( qpot_avrg(iblock) - qpotential_avrg )**two
         sigma_qtotalenergy = sigma_qtotalenergy + ( qtote_avrg(iblock) - qtotalenergy_avrg )**two
         sigma_qtotalenergy_v2 = sigma_qtotalenergy_v2 + &
              ( qtote_v2_avrg(iblock) - qtotalenergy_v2_avrg )**two
         sigma_radius2 = sigma_radius2 + &
              ( r2_avrg(iblock) - radius2_avrg )**two
      end do
      sigma_kinetic = dsqrt( sigma_kinetic / dfloat(nblocks) ) 
      sigma_potential = dsqrt( sigma_potential / dfloat(nblocks) )
      sigma_totalenergy = dsqrt( sigma_totalenergy / dfloat(nblocks) )
      sigma_qkinetic = dsqrt( sigma_qkinetic / dfloat(nblocks) )
      sigma_qkinetic_v2 = dsqrt( sigma_qkinetic_v2 / dfloat(nblocks) )
      sigma_qpotential = dsqrt( sigma_qpotential / dfloat(nblocks) )
      sigma_qtotalenergy = dsqrt( sigma_qtotalenergy / dfloat(nblocks) )
      sigma_qtotalenergy_v2 = dsqrt( sigma_qtotalenergy_v2 / dfloat(nblocks) )
      sigma_radius2 = dsqrt( sigma_radius2 / dfloat(nblocks) )
      
! outputs

      ! momenta distribution

      if (start) then
      
         open(unit=21, file="pdistro-o.dat")
         open(unit=22, file="pdistro-h.dat")
         open(unit=23, file="pdistro-h2o.dat")

         ! p distribution of oxygens
         do j = 1, nbin
            pc = pmin_o + (dfloat(j)-half)*deltap_o
            gaussian = one / dsqrt( two * pi ) / sigma_o * dexp( -pc*pc /two /sigma_o**two )
            write(21,*) pc, pdistro_o(j), gaussian
         end do

         ! p distribution of hydrogens
         do j = 1, nbin
            pc = pmin_h + (dfloat(j)-half)*deltap_h
            gaussian = one / dsqrt( two * pi ) / sigma_h * dexp( -pc*pc /two /sigma_h**two )
            write(22,*) pc, pdistro_h(j), gaussian
         end do
      
         ! p distribution of water molecules
         do j = 1, nbin
            pc = pmin_h2o + (dfloat(j)-half)*deltap_h2o
            gaussian = one / dsqrt( two * pi ) / sigma_h2o * dexp( -pc*pc /two /sigma_h2o**two )
            write(23,*) pc, pdistro_h2o(j), gaussian
         end do

         close(21)
         close(22)
         close(23)

      end if
      
      ! Final averages

      write(20,*)
      write(20,*) 'Temperature average:', temperature_avrg / temperature_target
      !write(*,*) 'Kinetic energy average:', kinetic_avrg / ( three / two * kT ) / dfloat(nReplicas)
      !write(*,*) 'Potential energy average:', potential_avrg / ( three / two * kT ) / &
      !     dfloat(nReplicas)

      if (nReplicas.gt.1) then
         write(20,*)
         write(20,*) 'Quantum energies per molecule (in kcalmol)'
         write(20,*) 'Total:', qtotalenergy_avrg / dfloat(n_mols) / kcalmol_to_au, &
              sigma_qtotalenergy / dfloat(n_mols) / kcalmol_to_au
         write(20,*) 'Total_v2:', qtotalenergy_v2_avrg / dfloat(n_mols) / kcalmol_to_au, &
              sigma_qtotalenergy_v2 / dfloat(n_mols) / kcalmol_to_au         
         write(20,*) 'Kinetic:', qkinetic_avrg / dfloat(n_mols) / kcalmol_to_au, &
              sigma_qkinetic / dfloat(n_mols) / kcalmol_to_au
         write(20,*) 'Kinetic_v2:', qkinetic_v2_avrg / dfloat(n_mols) / kcalmol_to_au, &
              sigma_qkinetic_v2 / dfloat(n_mols) / kcalmol_to_au
         write(20,*) 'Potential:', qpotential_avrg / dfloat(n_mols) / kcalmol_to_au, &
              sigma_qpotential / dfloat(n_mols) / kcalmol_to_au
      end if
      write(20,*)
      write(20,*) 'Extended system classical energies per molecule (in kcalmol)'
      write(20,*) 'Total:', totalenergy_avrg / dfloat(n_mols) / kcalmol_to_au, &
           sigma_totalenergy / dfloat(n_mols) / kcalmol_to_au
      write(20,*) 'Kinetic:', kinetic_avrg / dfloat(n_mols) / kcalmol_to_au, &
           sigma_kinetic / dfloat(n_mols) / kcalmol_to_au
      write(20,*) 'Potential:', potential_avrg / dfloat(n_mols) / kcalmol_to_au, &
           sigma_potential / dfloat(n_mols) / kcalmol_to_au

      ! squared mean radius and gyration radius
      
      write(20,*)
      write(20,*) 'Squared mean radius (in Angstrom)'
      write(20,*) dsqrt(radius2_avrg) / ang_to_au, half / dsqrt(radius2) * sigma_radius2 / ang_to_au
      
      gyration2_o = zero
      gyration2_h = zero
      do imol = 1, n_mols
         do j = 1, 3
            iat = j + 3*(imol-1)
            if (j.eq.1) then
               gyration2_o = gyration2_o + gyration2_avrg(iat)
            else
               gyration2_h = gyration2_h + gyration2_avrg(iat)
            end if
         end do
      end do
      gyration2_o = gyration2_o / dfloat( n_mols )
      gyration2_h = gyration2_h / ( two*dfloat(n_mols) )
      sigma_gyration2_o = zero
      sigma_gyration2_h = zero
      do imol = 1, n_mols
         do j = 1, 3
            iat = j + 3*(imol-1)
            if (j.eq.1) then
               sigma_gyration2_o = sigma_gyration2_o + ( gyration2(iat) - gyration2_o )**two
            else
               sigma_gyration2_h = sigma_gyration2_h + ( gyration2(iat) - gyration2_h )**two
            end if
         end do
      end do
      sigma_gyration2_o = dsqrt( sigma_gyration2_o / dfloat(n_mols) )
      sigma_gyration2_h = dsqrt( sigma_gyration2_h / ( two*dfloat(n_mols) ) )
      write(20,*) 'Gyration radius of O and H (in Angstrom)'
      write(20,*) dsqrt(gyration2_o) / ang_to_au, &
           half / dsqrt(gyration2_o) * sigma_gyration2_o / ang_to_au, &
           dsqrt(gyration2_h) / ang_to_au, &
           half / dsqrt(gyration2_h) * sigma_gyration2_h / ang_to_au

      ! Lindemann index

      lindemann_avrg = zero
      do imol = 1, n_mols
         do jmol = imol+1, n_mols
            lindemann_avrg = lindemann_avrg + &
                 dsqrt( rij2_avrg(imol,jmol) - rij_avrg(imol,jmol)**two ) / rij_avrg(imol,jmol)
         end do
      end do
      lindemann_avrg = lindemann_avrg * two / ( n_mols * (n_mols-1) )
      sigma_lindemann = zero
      do imol = 1, n_mols
         do jmol = imol+1, n_mols
            lindemann = dsqrt( rij2_avrg(imol,jmol) - rij_avrg(imol,jmol)**two ) &
                 / rij_avrg(imol,jmol)
            sigma_lindemann = sigma_lindemann + ( lindemann - lindemann_avrg )**two
         end do
      end do
      sigma_lindemann = dsqrt( sigma_lindemann * two / ( n_mols * (n_mols-1) ) )

      write(20,*)
      write(20,*) 'Lindemann index' 
      write(20,*) lindemann, sigma_lindemann

      ! Averaged centroid 
      
      open(unit=30, file='centroid.xyz')
      write(30,*) n_atoms
      write(30,*)
      do iat = 1, n_atoms
         if (mod(iat,3).eq.1) then
            write(30,*) 'O', centroid_avrg(1:3,iat) / ang_to_au ! convert a.u. to Angstrom
         else
            write(30,*) 'H', centroid_avrg(1:3,iat) / ang_to_au ! convert a.u. to Angstrom
         end if
      end do
      close(30)

      ! Polymer positions of atoms over different replicas

      call PolymerDistribution( state_chain, polymer )     

      open(unit=31, file='polymer.xyz')
      write(31,*) n_atoms*nReplicas
      write(31,*)
      do i = 1, nReplicas
         do iat = 1, n_atoms
            if (mod(iat,3).eq.1) then
               write(31,*) 'O', polymer(1:3,iat,i) / ang_to_au ! convert a.u. to Angstrom
            else
               write(31,*) 'H', polymer(1:3,iat,i) / ang_to_au ! convert a.u. to Angstrom
            end if
         end do
      end do
      close(31)

      ! Spatial distribution of the centroid

      !xmax = three * radius0
      xmax = two * radius0
      xmin = -xmax
      dx = (xmax-xmin)/dfloat(nbinx)
      
      open(unit=32, file='xdistro-o.dat')
      open(unit=33, file='xdistro-h.dat')
      do i1 = 0, nbinx-1
         x1c = ( xmin + ( dfloat(i1)+half )*dx ) / ang_to_au
         do i2 = 0, nbinx-1
            x2c = ( xmin + ( dfloat(i2)+half )*dx ) / ang_to_au
            write(32,'(5f10.5)') x1c, x2c, density_o_avrg(1,i1+1,i2+1), &
                 density_o_avrg(2,i1+1,i2+1), density_o_avrg(3,i1+1,i2+1)
            write(33,'(5f10.5)') x1c, x2c, density_h_avrg(1,i1+1,i2+1), &
                 density_h_avrg(2,i1+1,i2+1), density_h_avrg(3,i1+1,i2+1)
         end do ! end iy

         write(32,*)
         write(33,*)
         
      end do ! end ix
      close(32)
      close(33)
      
! write restart file
      
      call writeRestartChain( state_chain, newrestartFile, nStep )
      
! deallocate

      deallocate( mass )
      deallocate( position0 )
      deallocate( momentum )
      deallocate( centroid )
      deallocate( centroid_avrg )      
      deallocate( polymer )      
      deallocate( gyration2 )
      deallocate( gyration2_avrg )
      deallocate( rij )
      deallocate( rij2 )
      deallocate( rij_avrg )
      deallocate( rij2_avrg )                  
      call deleteStateChain( state_chain )

! end program

      call cpu_time(tfinish_cpu)
      cputime = tfinish_cpu - tstart_cpu
      call system_clock( count_1, count_rate, count_max )
      walltime = dfloat(count_1)/dfloat(count_rate) - dfloat(count_0)/dfloat(count_rate)
      !walltime = omp_get_wtime() - walltime
      write(*,*)
      write(*,*) 'CPU time (seconds, minutes, hours):'
      write(*,'(3f10.3)') cputime, cputime/60.0_dp, cputime/3600.0_dp
      write(20,*)
      write(20,*) 'CPU time (seconds, minutes, hours):'
      write(20,'(3f10.3)') cputime, cputime/60.0_dp, cputime/3600.0_dp
      write(*,*)
      write(*,*) 'Wall clock time (seconds, minutes, hours):'
      write(*,'(3f10.3)') walltime, walltime/60.0_dp, walltime/3600.0_dp
      write(20,*)
      write(20,*) 'Wall clock time (seconds, minutes, hours):'
      write(20,'(3f10.3)') walltime, walltime/60.0_dp, walltime/3600.0_dp            

      close(20) ! close logfile            

contains
  
!*****************************************************************************
subroutine computeForces( state_chain, temperature_target, gyration2, centroid )

!*****************************************************************************
!
!  Project: MolecularDynamics
!
!  Created on: March 2019 by Alfonso Gijón
!
! ---
! Copyright (C) 2018       Eduardo R. Hernandez - ICMM
!  This file is distributed under the terms of the
!  GNU General Public License: see COPYING in the top directory
!  or http://www.gnu.org/copyleft/gpl.txt .
! ---
!
! This subroutine receives a chain of states to compute intra and inter-replica
! energies and forces, and update them.
!
! Also compute the gyration radius and the centroid of the atoms
!
!*****************************************************************************
!  modules used

!*****************************************************************************
!  No implicits please!
   
      implicit none

!*****************************************************************************
!  arguments

      type (StateChain_type), intent (INOUT) :: state_chain

      real (dp), intent(IN) :: temperature_target

      real (dp), dimension(:), intent (OUT) :: gyration2

      real (dp), dimension (:,:), intent(OUT) :: centroid

!*****************************************************************************
!  local variables

      integer i, j, inext
      integer imol, iat

      integer nAtoms
      integer nReplicas
      integer nmols

      real (dp) :: beta, beta2

      real (dp) potential_energy
      real (dp) qPotentialEnergy
      real (dp) qKineticEnergy
      real (dp) qKineticEnergy_v2
      
      real (dp) vintra_total
      real (dp) vspring, vspring_total
      real (dp) vspring_v2, vspring_total_v2
      real (dp) :: kfactor, kspring
      
      real (dp), dimension (3) :: rspring
      real (dp), dimension (3) :: fspring
      real (dp), dimension (3) :: deltar

      real (dp), dimension (3) :: a, b, c
      real (dp), allocatable, dimension (:,:,:) :: position
      real (dp), allocatable, dimension (:,:,:) :: force
      real (dp), allocatable, dimension (:) :: mass
      real (dp), allocatable, dimension (:) :: vintra      

!*****************************************************************************
!  start subroutine

      call getStateChain( state_chain, nReplicas = nReplicas, nAtoms = nAtoms )

      nmols = nAtoms/3
      
      allocate( position( 3, nAtoms, nReplicas ) )
      allocate( force( 3, nAtoms, nReplicas ) )
      allocate( mass( nAtoms ) )
      allocate( vintra( nReplicas ) )      

      call getStateChain( state_chain, mass = mass )
      
      ! compute intra-replica energy and forces
      
      vintra = zero

      !$OMP PARALLEL &
      !$OMP PRIVATE( i )
      !$OMP DO
      do i = 1, nReplicas

         ! get position from each replica of the chain

         call getStateChain( state_chain, iReplica = i, position = position(:,:,i) )

         call evaluate_energy_SPCF( nmols, position(:,:,i), vintra(i), &
              force(:,:,i) )

      end do
      !$OMP END DO
      !$OMP END PARALLEL
      
      ! compute centroid position and total intra-replica energy

      vintra_total = zero
      centroid = zero         
      
      do i = 1, nReplicas

         vintra_total = vintra_total + vintra(i)
         centroid(:,:) = centroid(:,:) + position(:,:,i)
         
      end do
      
      centroid = centroid / dfloat(nReplicas)

      ! Calculate vspring, using the internal forces and the path integral virial theorem

      vspring_total_v2 = zero
      
      gyration2 = zero

      do i = 1, nReplicas

         vspring_v2 = zero

         do j = 1, nAtoms

            !deltar = position(:,j,i) - position(:,j,1)
            deltar = position(:,j,i) - centroid(:,j)
            vspring_v2 = vspring_v2 + dot_product( deltar, force(:,j,i) )
            
            ! gyration radius squared
            gyration2(j) = gyration2(j) + dot_product( deltar, deltar )
            
         end do

         vspring_total_v2 = vspring_total_v2 + vspring_v2
         
      end do

      vspring_total_v2 = vspring_total_v2 * half / dfloat(nReplicas)

      gyration2 = gyration2 / dfloat(nReplicas)

      ! In the the polymer Hamiltonian, the internal potential is reduced by a 1/nReplicas factor
      
      vintra_total = vintra_total / dfloat(nReplicas)
      force = force / dfloat(nReplicas)
      
      ! add inter-replica energy and forces (in a.u.)

      beta = one / ( boltzmann_k * temperature_target )
      beta2 = beta*beta

      kfactor = dfloat(nReplicas) / beta2

      vspring_total = zero

      do i = 1, nReplicas

         inext = i + 1
         if (inext == nReplicas+1) inext = 1

         vspring = zero

         do j = 1, nAtoms

            kspring = mass(j) * kfactor ! it depends on mass, number of replicas and temperature
            rspring = position(:,j,inext) - position(:,j,i)
            vspring = vspring + half * kspring * dot_product( rspring, rspring )
            
            fspring = kspring * rspring ! factor 2 is compensated with factor 1/2
            force(:,j,i) = force(:,j,i) + fspring
            force(:,j,inext) = force(:,j,inext) - fspring
                  
         end do

         vspring_total = vspring_total + vspring

      end do

      do i = 1, nReplicas

         call setStateChain( state_chain, iReplica = i, force = force(:,:,i) )
         
      end do

      ! update classical potential energy of the chain

      potential_energy = vintra_total + vspring_total
      
      call setStateChain( state_chain, potentialEnergy = potential_energy )
      
      ! update quantum energies of the chain

      qPotentialEnergy = vintra_total
      qKineticEnergy = three * half * dfloat(nReplicas*nAtoms) / beta - vspring_total
      qKineticEnergy_v2 = three * half * dfloat(nAtoms) / beta - vspring_total_v2
      
      call setStateChain( state_chain, qPotentialEnergy = qPotentialEnergy, &
           qKineticEnergy = qKineticEnergy, qKineticEnergy_v2 = qKineticEnergy_v2 )

      ! deallocate local variables
      
      deallocate( position )
      deallocate( force )
      deallocate( mass )

end subroutine computeForces    

end program example_PI_water_SPCF
